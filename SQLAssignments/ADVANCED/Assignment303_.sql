USE ass303
GO

CREATE TABLE SKILL (
	SkillNo INT PRIMARY KEY,
	SkillName NVARCHAR(30) NOT NULL,
	Note VARCHAR(255)
);
GO

CREATE TABLE DEPARTMENT (
	DeptNo INT PRIMARY KEY,
	DeptName NVARCHAR(30) NOT NULL,
	Note VARCHAR(255)
);
GO

CREATE TABLE EMPLOYEE (
	EmpNo INT PRIMARY KEY,
	EmpName NVARCHAR(30) NOT NULL,
	BirthDay DATE NOT NULL,
	Email NVARCHAR(30) NOT NULL UNIQUE,
	DeptNo INT NOT NULL,
	MgrNo INT NOT NULL DEFAULT 0,
	StartDate DATE NOT NULL,
	Salary MONEY NOT NULL,
	[Level] INT NOT NULL,
	[Status] INT NOT NULL DEFAULT 0,
	Note VARCHAR(255),
	CHECK ([Level]>=(0) AND [Level]<=(7)),
	CHECK ([Status]>=(0) AND [Status]<=(2)),
	FOREIGN KEY (DeptNo) REFERENCES DEPARTMENT(DeptNo),
);
GO

CREATE TABLE EMP_SKILL (
	SkillNo INT NOT NULL,
	EmpNo INT NOT NULL,
	SkillLevel INT NOT NULL,
	RegDate DATE NOT NULL,
	[Description] VARCHAR(255),
	CHECK (SkillLevel>=1 AND SkillLevel<=3),
	FOREIGN KEY (SkillNo) REFERENCES SKILL(SkillNo),
	FOREIGN KEY (EmpNo) REFERENCES EMPLOYEE(EmpNo)
);
GO


INSERT INTO SKILL(SkillNo, SkillName, Note) VALUES
	(1, 'C++', null),
	(2, '.NET', null),
	(3, 'Java', null),
	(4, 'ReactJS', null),
	(5, 'Accounting', null),
	(6, 'MS office', null),
	(7, 'Ruby', null),
	(8, 'HTML/CSS', null);
GO

INSERT INTO DEPARTMENT (DeptNo, DeptName, Note) VALUES
	(101, 'IT', null),
	(102, 'Human Resources', null),
	(103, 'Accounting and Finance', null),
	(201, 'Marketing', null),
	(202, 'Research and Development', null),
	(203, 'Operations Management', null),
	(301, 'Sales', null),
	(302, 'Quality Assurance', null);
GO

INSERT INTO EMPLOYEE (EmpNo, EmpName, BirthDay, Email, DeptNo, MgrNo, StartDate, Salary, [Level], [Status], Note) VALUES
	(1001, 'Harry', '2001-02-03', 'harry@gmail.com', 101, 0, '2020-01-01', 2000, 4, 0, null),
	(1002, 'Jane', '1999-01-03', 'jane@gmail.com', 103, 0, '2019-01-01', 3000, 5, 0, null),
	(1003, 'Jean', '2002-12-10', 'jean@gmail.com', 301, 1002, '2023-08-01', 300, 1, 1, null),
	(1004, 'Bill', '2001-06-03', 'bill@gmail.com', 101, 1001, '2020-01-01', 1000, 2, 0, null),
	(1005, 'Coley', '1995-06-13', 'coley@gmail.com', 202, 0, '2018-01-01', 3000, 6, 0, null),
	(1006, 'Mike', '1997-06-30', 'mike@gmail.com', 202, 1005, '2019-01-01', 2200, 5, 0, null),
	(1007, 'Dane', '2000-04-30', 'dane@gmail.com', 202, 1005, '2020-01-01', 1500, 3, 0, null),
	(1008, 'Helen', '1993-04-30', 'helen@gmail.com', 201, 0, '2015-01-01', 3000, 6, 0, null)
GO

INSERT INTO EMP_SKILL (SkillNo, EmpNo, SkillLevel, RegDate, [Description]) VALUES
	(1, 1001, 3, '2020-06-01', null),
	(2, 1001, 3, '2020-06-01', null),
	(3, 1004, 2, '2020-06-01', null),
	(6, 1008, 3, '2015-06-01', null),
	(7, 1005, 3, '2019-06-01', null),
	(7, 1007, 3, '2020-06-01', null),
	(5, 1008, 2, '2020-01-01', null),
	(4, 1003, 1, '2023-08-01', null)
GO


-- 2. Specify name, email and department name of the employees that have been working at least six months.
SELECT e.EmpName, e.Email, d.DeptName 
FROM EMPLOYEE e JOIN DEPARTMENT d ON e.DeptNo = d.DeptNo AND DATEDIFF(MONTH, e.StartDate, GETDATE()) >= 6
GO

-- 3. Specify the names of the employees whose have either �C++� or �.NET� skills.
SELECT DISTINCT e.EmpName
FROM EMPLOYEE e JOIN EMP_SKILL es ON e.EmpNo = es.EmpNo
				JOIN SKILL s ON es.SkillNo = s.SkillNo
WHERE s.SkillName = 'C++' OR s.SkillName = '.NET'
GO

-- 4. List all employee names, manager names, manager emails of those employees.
SELECT DISTINCT e.EmpName, m.EmpName AS ManagerName, m.Email AS ManagerEmail
FROM EMPLOYEE e JOIN EMP_SKILL es ON e.EmpNo = es.EmpNo
				JOIN SKILL s ON es.SkillNo = s.SkillNo
				LEFT JOIN EMPLOYEE m ON e.MgrNo = m.EmpNo
WHERE s.SkillName = 'C++' OR s.SkillName = '.NET'
GO

-- 5. Specify the departments which have >=2 employees, print out the list of departments� employees right after each department.
SELECT d.DeptName, STRING_AGG(e.EmpName, ', ') AS Employees
FROM DEPARTMENT d JOIN EMPLOYEE e ON e.DeptNo = d.DeptNo
GROUP BY d.DeptName
HAVING COUNT(*) >= 2
GO

-- 6. List all name, email and skill number of the employees and sort ascending order by employee�s name.
SELECT e.EmpName, e.Email, COUNT(e.EmpNo) AS [Num of skills]
FROM EMPLOYEE e LEFT JOIN EMP_SKILL es ON e.EmpNo = es.EmpNo
GROUP BY e.EmpName, e.Email
ORDER BY e.EmpName ASC
GO

-- 7. Use SUB-QUERY technique to list out the different employees (include name, email, birthday) who are working and have multiple skills.
SELECT e.EmpName, e.Email, e.BirthDay
FROM EMPLOYEE e 
WHERE e.Status=0 AND e.EmpNo IN (
	SELECT es.EmpNo 
	FROM  EMP_SKILL es
	GROUP BY es.EmpNo
	HAVING COUNT(*) >= 2
);
GO

-- 8. Create a view to list all employees are working (include: name of employee and skill name, department name)
CREATE VIEW [Working Employees] AS
SELECT e.EmpName, s.SkillName, d.DeptName
FROM EMPLOYEE e LEFT JOIN EMP_SKILL es ON e.EmpNo = es.EmpNo
				LEFT JOIN SKILL s ON es.SkillNo = s.SkillNo
				LEFT JOIN DEPARTMENT d ON e.DeptNo = d.DeptNo
WHERE e.[Status] = 0
GO

SELECT * FROM [Working Employees]