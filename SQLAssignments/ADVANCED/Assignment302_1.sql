

USE ass302
GO

CREATE TABLE San_Pham (
	Ma_SP INT PRIMARY KEY,
	Ten_SP NVARCHAR(30) NOT NULL,
	Don_Gia MONEY NOT NULL
);

CREATE TABLE Khach_Hang (
	Ma_KH INT PRIMARY KEY,  
	Ten_KH NVARCHAR(30) NOT NULL,
	Phone_No NVARCHAR(10) NOT NULL, 
	Ghi_Chu NVARCHAR(MAX) NULL
);

CREATE TABLE Don_Hang (
	Ma_DH INT NOT NULL,
	Ngay_DH DATETIME NOT NULL,
	Ma_SP INT NOT NULL, 
	So_Luong INT NOT NULL,  
	Ma_KH INT NOT NULL,
	PRIMARY KEY (Ma_DH, Ma_SP, Ma_KH),
	FOREIGN KEY (Ma_SP) REFERENCES San_Pham(Ma_SP),
	FOREIGN KEY (Ma_KH) REFERENCES Khach_Hang(Ma_KH)
);


INSERT INTO San_Pham(Ma_SP, Ten_SP, Don_Gia) VALUES
	(1, 'iPhone 15', 750),
	(2, 'BMW i530', 50000),
	(3, 'HP Pavilion', 1000);
GO

INSERT INTO Khach_Hang (Ma_KH, Ten_KH, Phone_No, Ghi_Chu) VALUES
	(1, 'Biden', '0123456789', 'Confidential'),
	(2, 'Joe', '0123456779', null),
	(3, 'Trump', '0123454789', null);
GO

INSERT INTO Don_Hang(Ma_DH, Ngay_DH, Ma_SP, So_Luong, Ma_KH) VALUES
	(1, '2023-01-01', 3, 2, 1),
	(2, '2023-06-01', 2, 1, 2),
	(3, '2023-01-10', 1, 10, 3);
GO


CREATE VIEW [Bang Don Hang] AS
SELECT kh.Ten_KH, dh.Ngay_DH, sp.Ten_SP, dh.So_Luong, sp.Don_Gia * dh.So_Luong AS [Thanh_Tien]
FROM Don_Hang dh LEFT JOIN Khach_Hang kh ON dh.Ma_KH = kh.Ma_KH
				 LEFT JOIN San_Pham sp ON dh.Ma_SP = sp.Ma_SP 
GO

SELECT * FROM [Bang Don Hang]
GO


