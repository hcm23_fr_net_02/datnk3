USE ass302_2
GO

CREATE TABLE Department (
	Department_Number INT PRIMARY KEY, 
	Department_Name NVARCHAR(30) NOT NULL
);

CREATE TABLE Employee_Table (
	Employee_Number INT PRIMARY KEY,
	Employee_Name NVARCHAR(30) NOT NULL, 
	Department_Number INT NOT NULL,
	FOREIGN KEY (Department_Number) REFERENCES Department(Department_Number)
);

CREATE TABLE Skill_Table (
	Skill_Code INT PRIMARY KEY,
	Skill_Name NVARCHAR(30) NOT NULL
);

CREATE TABLE Employee_Skill_Table (
	Employee_Number INT NOT NULL, 
	Skill_Code INT NOT NULL, 
	[Date Registered] DATE NOT NULL,
	FOREIGN KEY (Employee_Number) REFERENCES Employee_Table(Employee_Number),
	FOREIGN KEY (Skill_Code) REFERENCES Skill_Table(Skill_Code),
);


INSERT INTO Department(Department_Number, Department_Name) VALUES
	(1, 'IT'),
	(2, 'HR'),
	(3, 'Marketing');
GO

INSERT INTO Skill_Table (Skill_Code, Skill_Name) VALUES
	(1, 'C++'),
	(2, 'C#'),
	(3, 'Accounting');
GO

INSERT INTO Employee_Table (Employee_Number, Employee_Name, Department_Number) VALUES
	(1, 'Joe', 1),
	(2, 'John', 1),
	(3, 'Henry', 2),
	(4, 'Mary', 3);
GO

INSERT INTO Employee_Skill_Table (Employee_Number, Skill_Code, [Date Registered]) VALUES
	(1, 1, '2020-01-06'),
	(2, 2, '2020-12-06'),
	(3, 3, '2021-01-15');
GO

-- 2.
-- Specify the names of the employees whose have skill of �Java� � give >=2 solutions:              
-- Use JOIN selection
-- Use sub query

SELECT e.Employee_Name 
FROM Employee_Table e JOIN Employee_Skill_Table es ON e.Employee_Number = es.Employee_Number
					  JOIN Skill_Table s ON es.Skill_Code = s.Skill_Code
WHERE s.Skill_Name = 'Java'
GO


SELECT e.Employee_Name
FROM Employee_Table e, Employee_Skill_Table es 
WHERE e.Employee_Number = es.Employee_Number AND es.Skill_Code IN (
	SELECT s.Skill_Code 
	FROM Skill_Table s 
	WHERE s.Skill_Name = 'Java'
)
GO

-- 3. Specify the departments which have >=3 employees, print out the list of departments� employees right after each department.
SELECT e.Department_Number, STRING_AGG(e.Employee_Name, ', ') AS Employees
FROM Employee_Table e
GROUP BY e.Department_Number
HAVING COUNT(*) >= 2
GO

-- 4. Use SUB-QUERY technique to list out the different employees (include employee number and employee names) who have multiple skills.
SELECT e.Employee_Number, e.Employee_Name 
FROM Employee_Table e 
WHERE e.Employee_Number IN (
	SELECT es.Employee_Number
	FROM Employee_Skill_Table es
	GROUP BY es.Employee_Number
	HAVING COUNT(*) >= 2
)

-- 5. Create a view to show different employees (with following information: employee number and employee name, department name) who have multiple skills.
SELECT e.Employee_Number, e.Employee_Name, d.Department_Name
FROM Employee_Table e JOIN Employee_Skill_Table es ON e.Employee_Number = es.Employee_Number
					  JOIN Department d ON e.Department_Number = d.Department_Number
GROUP BY e.Employee_Number, e.Employee_Name, d.Department_Name
HAVING COUNT(*) >= 2