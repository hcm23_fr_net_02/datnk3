-- Create table Employee, Status = 1: are working
CREATE TABLE [dbo].[Employee](
	[EmpNo] [int] NOT NULL
,	[EmpName] [nchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
,	[BirthDay] [datetime] NOT NULL
,	[DeptNo] [int] NOT NULL
, 	[MgrNo] [int]
,	[StartDate] [datetime] NOT NULL
,	[Salary] [money] NOT NULL
,	[Status] [int] NOT NULL
,	[Note] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
,	[Level] [int] NOT NULL
) ON [PRIMARY]

GO

ALTER TABLE Employee 
ADD CONSTRAINT PK_Emp PRIMARY KEY (EmpNo)
GO

ALTER TABLE [dbo].[Employee]  
ADD  CONSTRAINT [chk_Level] 
	CHECK  (([Level]=(7) OR [Level]=(6) OR [Level]=(5) OR [Level]=(4) OR [Level]=(3) OR [Level]=(2) OR [Level]=(1)))
GO
ALTER TABLE [dbo].[Employee]  
ADD  CONSTRAINT [chk_Status] 
	CHECK  (([Status]=(2) OR [Status]=(1) OR [Status]=(0)))

GO
ALTER TABLE [dbo].[Employee]
ADD Email NCHAR(30) 
GO

ALTER TABLE [dbo].[Employee]
ADD CONSTRAINT chk_Email CHECK (Email IS NOT NULL)
GO

ALTER TABLE [dbo].[Employee] 
ADD CONSTRAINT chk_Email1 UNIQUE(Email)

GO
ALTER TABLE Employee
ADD CONSTRAINT DF_EmpNo DEFAULT 0 FOR EmpNo

GO
ALTER TABLE Employee
ADD CONSTRAINT DF_Status DEFAULT 0 FOR Status

GO
CREATE TABLE [dbo].[Skill](
	[SkillNo] [int] IDENTITY(1,1) NOT NULL
,	[SkillName] [nchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
,	[Note] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

GO
ALTER TABLE Skill
ADD CONSTRAINT PK_Skill PRIMARY KEY (SkillNo)

GO
CREATE TABLE [dbo].[Department](
	[DeptNo] [int] IDENTITY(1,1) NOT NULL
,	[DeptName] [nchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL
,	[Note] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

GO
ALTER TABLE Department
ADD CONSTRAINT PK_Dept PRIMARY KEY (DeptNo)

GO
CREATE TABLE [dbo].[Emp_Skill](
	[SkillNo] [int] NOT NULL
,	[EmpNo] [int] NOT NULL
,	[SkillLevel] [int] NOT NULL
,	[RegDate] [datetime] NOT NULL
,	[Description] [nchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]

GO
ALTER TABLE Emp_Skill
ADD CONSTRAINT PK_Emp_Skill PRIMARY KEY (SkillNo, EmpNo)
GO

ALTER TABLE Employee  
ADD  CONSTRAINT [FK_1] FOREIGN KEY([DeptNo])
REFERENCES Department (DeptNo)

GO
ALTER TABLE Emp_Skill
ADD CONSTRAINT [FK_2] FOREIGN KEY ([EmpNo])
REFERENCES Employee([EmpNo])

GO
ALTER TABLE Emp_Skill
ADD CONSTRAINT [FK_3] FOREIGN KEY ([SkillNo])
REFERENCES Skill([SkillNo])

GO

-- Check
SELECT * FROM Department
GO

ALTER TABLE Employee NOCHECK CONSTRAINT ALL
DELETE FROM Employee
ALTER TABLE Employee CHECK CONSTRAINT ALL
GO

SELECT * FROM Employee
GO

SELECT * FROM Skill
GO

SELECT * FROM Emp_Skill
GO

-- Question 1: Add at least 8 records into each created tables.
INSERT INTO Department(DeptName) VALUES ('Human Resources')
INSERT INTO Department(DeptName) VALUES ('IT')
INSERT INTO Department(DeptName) VALUES ('Accounting and Finance')
INSERT INTO Department(DeptName) VALUES ('Marketing')
INSERT INTO Department(DeptName) VALUES ('Research and Development')
INSERT INTO Department(DeptName) VALUES ('Operations Management')
INSERT INTO Department(DeptName) VALUES ('Sales')
INSERT INTO Department(DeptName) VALUES ('Quality Assurance')
GO

INSERT INTO Employee(EmpNo, EmpName, BirthDay, DeptNo, MgrNo, StartDate, Salary, Status, Level, Email) 
	VALUES (1, 'John Cena', N'1977-04-23', 2, null, N'2020-01-01', 5000, 1, 5, N'johncena@fpt.com'),
			(2, 'Nguyen Ke Dat', N'2001-06-30', 2, 1, N'2023-09-23', 300, 1, 1, N'datnk3@fpt.com'),
			(3, 'Camila Cabello', N'1997-03-03', 1, null, N'2021-01-21', 3000, 1, 3, N'caca@fpt.com'),
			(4, 'Shawn Mendes', N'1998-08-08', 1, 3, N'2022-04-01', 1000, 1, 2, N'shades@fpt.com'),
			(5, 'Mark Zuckerberg', N'1984-05-14', 2, null, N'2023-01-01', 4000, 1, 4, N'markzuck@fpt.com'),
			(6, 'Barack Obama', N'1961-08-04', 8, null, N'2019-09-23', 6000, 1, 7, N'obamna@fpt.com'),
			(7, 'Son Tung', N'1994-07-05', 7, 6, N'2023-06-01', 750, 1, 2, N'tungnui@fpt.com'),
			(8, 'Chi Pu', N'1993-06-14', 3, 6, N'2022-12-01', 1500, 1, 2, N'chipu@fpt.com')
GO

INSERT INTO Skill(SkillName) VALUES
	('.Net'), ('ReactJS'), ('MBA'), ('HTML/CSS'), ('MS Office'), ('Law'), ('Art'), ('Influence')
GO

INSERT INTO Emp_Skill(SkillNo, EmpNo, SkillLevel, RegDate, Description) VALUES (1, 1, 5, N'2020-01-01', 'Expert in .Net')
INSERT INTO Emp_Skill(SkillNo, EmpNo, SkillLevel, RegDate, Description) VALUES (1, 2, 1, N'2023-09-23', 'Fresher in .Net')
INSERT INTO Emp_Skill(SkillNo, EmpNo, SkillLevel, RegDate, Description) VALUES (2, 2, 2, N'2023-09-23', 'One year exe in ReactJS')
INSERT INTO Emp_Skill(SkillNo, EmpNo, SkillLevel, RegDate, Description) VALUES (7, 3, 5, N'2022-04-01', 'Famous singer')
INSERT INTO Emp_Skill(SkillNo, EmpNo, SkillLevel, RegDate, Description) VALUES (7, 4, 5, N'2023-01-01', 'Famous singer')
INSERT INTO Emp_Skill(SkillNo, EmpNo, SkillLevel, RegDate, Description) VALUES (4, 5, 6, N'2023-01-01', 'Expert in HTML/CSS')
INSERT INTO Emp_Skill(SkillNo, EmpNo, SkillLevel, RegDate, Description) VALUES (8, 6, 6, N'2023-01-01', 'President of the USA')
INSERT INTO Emp_Skill(SkillNo, EmpNo, SkillLevel, RegDate, Description) VALUES (6, 6, 6, N'2023-01-01', 'Expert in law')
GO

-- Question 2: Specify the name, email and department name of the employees that have been working at least six months.
SELECT e.EmpName, e.Email, d.DeptName 
FROM dbo.Employee e, dbo.Department d 
WHERE e.DeptNo = d.DeptNo AND DATEDIFF(MONTH, e.StartDate, GETDATE()) >= 6
GO

-- Question 3: Specify the names of the employees whore have either �C++� or �.NET� skills.
SELECT e.EmpName 
FROM dbo.Employee e, dbo.Emp_Skill es 
WHERE e.EmpNo = es.EmpNo AND es.SkillNo IN (
	SELECT s.SkillNo 
	FROM dbo.Skill s 
	WHERE s.SkillName = '.NET' OR s.SkillName = 'C++'
)

-- Question 4: List all employee names, manager names, manager emails of those employees.
SELECT e.EmpName, m.EmpName AS ManagerName, m.Email 
FROM dbo.Employee e LEFT JOIN dbo.Employee m ON e.MgrNo = m.EmpNo 
WHERE e.EmpName IN (
	SELECT e.EmpName 
	FROM dbo.Employee e, dbo.Emp_Skill es 
	WHERE e.EmpNo = es.EmpNo AND es.SkillNo IN (
		SELECT s.SkillNo 
		FROM dbo.Skill s 
		WHERE s.SkillName = '.NET' OR s.SkillName = 'C++'
	)
)
GO

-- Question 5: Specify the departments which have >=2 employees, print out the list of departments� employees right after each department.

DECLARE @deptList TABLE(DeptNo INT, DeptName NCHAR(30))

INSERT INTO @deptList SELECT d.DeptNo, d.DeptName FROM dbo.Department d, dbo.Employee e WHERE d.DeptNo = e.DeptNo GROUP BY d.DeptNo, d.DeptName HAVING COUNT(*) >= 2

SELECT * FROM @deptList
GO

-- Question 6: List all name, email and number of skills of the employees and sort ascending order by employee�s name.
SELECT e.EmpName, e.Email, COUNT(es.SkillNo) AS [Num of skills]
FROM dbo.Employee e LEFT JOIN dbo.Emp_Skill es ON es.EmpNo = e.EmpNo 
GROUP BY e.EmpName, e.Email 
ORDER BY e.EmpName ASC
GO

-- Question 7: Use SUB-QUERY technique to list out the different employees (include name, email, birthday) who are working and have multiple skills.
SELECT e.EmpName, e.Email, e.BirthDay 
FROM dbo.Employee e 
WHERE e.Status = 1 AND e.EmpNo IN (
	SELECT e.EmpNo 
	FROM dbo.Employee e LEFT JOIN dbo.Emp_Skill es ON e.EmpNo = es.EmpNo 
	GROUP BY e.EmpNo 
	HAVING COUNT(*) >= 2
)
GO

-- Question 8: Create a view to list all employees are working (include: name of employee and skill name, department name).
CREATE VIEW [Working Employee] AS
SELECT e.EmpName, s.SkillName, d.DeptName
FROM Employee e JOIN Emp_Skill es ON e.EmpNo = es.EmpNo
				JOIN Skill s ON es.SkillNo = s.SkillNo
				JOIN Department d ON e.DeptNo = d.DeptNo
WHERE e.Status = 1
GO

SELECT * FROM [Working Employee]
GO