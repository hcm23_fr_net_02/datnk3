## Mock test in 29/9/2023

Cở sở dữ liệu quản lý bán hàng

KhachHang(MaKH, HoTen, DiaChi, SoDT, NgSinh, Doanhso, NgDk)
NhanVien(MaNV, HoTen, SoDT, NgVaoLam)
SanPham(MaSP, TenSP, DVT, NuocSX, Gia)
HoaDon(SoHD, NgHD, MaKH, MaNV, TriGia)
CTHD(SoHD, MaSP, SL)

Tạo bảng và thêm vào mỗi bảng 3 dòng dữ liệu

Câu 1: In ra danh sách các sản phẩm(MaSP, TenSP) do "Viet Nam" sản xuất hoặc các sản phẩm bán trong ngày 23/052023\
Câu 2: In ra danh sách các sản phẩm (MaSP, TenSP) không bán được\
Câu 3: Tìm số hóa đon đã mua tất cả các sản phẩm do Viet Nam sản xuất\
