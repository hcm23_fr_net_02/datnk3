﻿
delegate int MathOperation(int a, int b);

delegate int Custom(int number);

class Program
{
    static void Main(string[] args)
    {
        MathOperation mathOperation = new(Addition);
        Console.WriteLine(mathOperation(3, 6));
        Console.WriteLine(PerformOperation(20, 23, Multiplication));


        PrintResult(3, Double);
        PrintResult(10, Triple);
        PrintResult(20, Square);
        PrintResult(15, Cube);

    }

    public static int Addition(int a, int b) {  return a + b; }
    public static int Subtraction(int a, int b) { return a - b;}
    public static int Multiplication(int a, int b) { return a * b;}
    public static int Division(int a, int b) { return a / b;}
    
    public static int PerformOperation(int a, int b, MathOperation mathOperation)
    {
        return mathOperation(a, b);
    }

    public static int Double(int number) { return number * 2; }
    public static int Triple(int number) { return number * 3; }
    public static int Square(int number) { return number * number; }
    public static int Cube(int number) { return number * number * number; }
    public static void PrintResult(int number, Custom operation)
    {
        Console.WriteLine(operation(number));
    }
}