﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StackQueue
{
    public class DataPair<T, U>
    {
        public T Data1 { get; set; }
        public U Data2 { get; set; }
        public DataPair(T data1, U data2)
        {
            Data1 = data1;
            Data2 = data2;
        }
    }
}
