﻿using StackQueue;

class Program
{
    static void Main(string[] args)
    {
        // Stack
        MyStack<int> myStack = new(5);

        myStack.Push(5);
        myStack.Push(10);
        myStack.Push(20);

        MyStack<string> myStack1 = new(5);
        myStack1.Push("Joe");
        myStack1.Push("Harry");
        myStack1.Push("Gordor");
        myStack1.Pop();

        MyStack<Person> myStack2 = new(5);
        myStack2.Push(new Person("Gordor", 0123456789));
        myStack2.Pop();
        Console.WriteLine(myStack2.IsEmpty());
        try
        {
            myStack2.Pop();
        } catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }

        MyStack<Student> myStack3 = new(2);
        myStack3.Push(new Student(1, "James"));
        myStack3.Push(new Student(2, "Kane"));
        try
        {
            myStack3.Push(new Student(3, "Candace"));
        } catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }

        // Queue
        MyQueue<int> myQueue = new(5);
        myQueue.Enqueue(5);
        myQueue.Enqueue(10);
        myQueue.Enqueue(20);
        Console.WriteLine(myQueue.Dequeue());
        Console.WriteLine(myQueue.Count);

        MyQueue<string> myQueue2 = new(3);
        myQueue2.Enqueue("hello");
        myQueue2.Enqueue("konichiwa");
        myQueue2.Enqueue("xi xi");
        try
        {
            myQueue2.Enqueue("ohaiyo");
        } catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }

        MyQueue<Person> myQueue3 = new(3);
        myQueue3.Enqueue(new Person("Gordor", 0123456789));
        myQueue3.Dequeue();
        try
        {
            myQueue3.Dequeue();
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }

        // DataPair
        var pair1 = new DataPair<int, string>(1, "Norwegian Wood");
    }
}

public class Person
{
    public string Name { get; set; }
    public int PhoneNum { get; set; }

    public Person(string name, int phone)
    {
        Name = name;
        PhoneNum = phone;
    }
}

struct Student
{
    public int Id { get; set; }
    public string Name { get; set; }
    public Student(int id, string name)
    {
        Id = id;
        Name = name;
    }
}