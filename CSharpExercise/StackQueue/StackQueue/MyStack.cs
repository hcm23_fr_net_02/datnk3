﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StackQueue
{
    public class MyStack<T>
    {
        private readonly int _defaultCapacity;
        public const string StackEmptyErrorMessage = "Stack is empty";

        private T[] Items;

        private int LastItemIndex;
        public MyStack(int num) 
        {
            _defaultCapacity = num;
            Items = new T[_defaultCapacity];
            LastItemIndex = -1;
        }

        public void Clear()
        {
            LastItemIndex = -1;
            Count = _defaultCapacity;
        }

        public bool Contains(T item)
        {
            return Array.IndexOf(Items, item, 0, LastItemIndex+1) >= 0;
        }

        public bool IsEmpty()
        {
            return (LastItemIndex == -1);
        }

        public T Peek()
        {
            if (LastItemIndex < 0)
            {
                throw new InvalidOperationException(StackEmptyErrorMessage);
            }

            return Items[LastItemIndex];
        }

        public T Pop()
        {
            if (LastItemIndex < 0)
            {
                throw new InvalidOperationException(StackEmptyErrorMessage);
            }

            return Items[LastItemIndex--];
        }

        public void Push(T item)
        {
            if (LastItemIndex == Count - 1)
            {
                Count *= 2;
            }

            Items[++LastItemIndex] = item;
        }

        public int Count
        {
            get => Items.Length;
            private set => Array.Resize(ref Items, value);
        }


    }
}
