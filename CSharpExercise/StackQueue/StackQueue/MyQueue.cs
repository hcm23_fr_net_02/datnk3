﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StackQueue
{
    public class MyQueue<T>
    {
        private int _capacity;
        public const string QueueEmptyErrorMessage = "Queue is empty";
        public const string QueueOverflowErrorMessage = "Queue is full";

        private T[] Items;
        private int FirstItemIndex;
        public int Count
        {
            get;
            set;
        }

        public MyQueue(int num)
        {
            _capacity = num;
            Items = new T[num];
            FirstItemIndex = -1;
            Count = 0;
        }

        public void Enqueue(T item)
        {
            if (Count == _capacity)
            {
                throw new InvalidOperationException(QueueOverflowErrorMessage);
            }

            FirstItemIndex++;
            Items[FirstItemIndex] = item;
            Count++;

            if (FirstItemIndex >= Items.Length)
            {
                FirstItemIndex = 0;
            }
        }

        public T Dequeue()
        {
            if (Count == 0)
            {
                throw new InvalidOperationException(QueueEmptyErrorMessage);
            }

            Count--;
            return Items[FirstItemIndex-Count];
        }

        public T Peek()
        {
            if (Count == 0)
            {
                throw new InvalidOperationException(QueueEmptyErrorMessage);
            }

            return Items[FirstItemIndex];
        }

        public bool IsEmpty()
        {
            return Count == 0;
        }

        public void Clear()
        {
            Count = 0;
            FirstItemIndex = -1;
        }
    }
}
