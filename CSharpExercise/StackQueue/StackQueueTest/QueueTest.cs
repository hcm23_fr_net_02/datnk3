﻿using StackQueue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StackQueueTest
{
    [TestClass]
    public class QueueTest
    {
        [TestMethod]
        public void TestEnqueue()
        {
            // Arrange
            var queue = new MyQueue<int>(3);

            // Act
            queue.Enqueue(1);
            queue.Enqueue(2);
            queue.Enqueue(3);

            // Assert
            int expected = 3;
            Assert.AreEqual(expected, queue.Count);
        }

        [TestMethod]
        public void TestDequeue()
        {
            // Arrange
            var queue = new MyQueue<int>(3);

            // Act
            queue.Enqueue(1);
            queue.Enqueue(2);
            queue.Enqueue(3);

            // Assert
            int expected = 1;
            Assert.AreEqual(expected, queue.Dequeue());
        }

        [TestMethod]
        public void TestClearQueue()
        {
            // Arrange
            var queue = new MyQueue<int>(3);

            // Act
            queue.Enqueue(1);
            queue.Enqueue(2);
            queue.Enqueue(3);
            queue.Clear();

            // Assert
            bool expected = true;
            Assert.AreEqual(expected, queue.IsEmpty());
        }

        [TestMethod]
        public void TestPeekQueue()
        {
            // Arrange
            var queue = new MyQueue<int>(3);

            // Act
            queue.Enqueue(1);
            queue.Enqueue(2);
            queue.Enqueue(3);

            // Assert
            int expected = 3;
            Assert.AreEqual(expected, queue.Peek());
            Assert.AreEqual(3, queue.Count);
        }

        [TestMethod]
        public void TestEnqueueException()
        {
            // Arrange
            var queue = new MyQueue<int>(3);

            // Act and Assert
            try
            {
                queue.Enqueue(1);
                queue.Enqueue(2);
                queue.Enqueue(3);
                queue.Enqueue(4);
            }
            catch (InvalidOperationException ex)
            {
                StringAssert.Contains(ex.Message, MyQueue<int>.QueueOverflowErrorMessage);
                return;
            }
            Assert.Fail("The expected exception was not thrown.");
        }

        [TestMethod]
        public void TestDequeueException()
        {
            // Arrange
            var queue = new MyQueue<int>(3);

            // Act and Assert
            try
            {
                queue.Enqueue(1);
                queue.Dequeue();
                queue.Dequeue();
            }
            catch (InvalidOperationException ex)
            {
                StringAssert.Contains(ex.Message, MyQueue<int>.QueueEmptyErrorMessage);
                return;
            }
            Assert.Fail("The expected exception was not thrown.");
        }
    }
}
