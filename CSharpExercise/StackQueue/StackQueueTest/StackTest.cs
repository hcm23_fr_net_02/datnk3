using StackQueue;
using System;

namespace StackQueueTest
{
    [TestClass]
    public class StackTest
    {
        [TestMethod]
        public void TestPushPopStack()
        {
            // Arrange
            var stack = new MyStack<int>(3);

            // Act
            stack.Push(1);
            stack.Push(2);
            stack.Push(3);

            // Assert
            int expected = 3;
            Assert.AreEqual(expected, stack.Pop());
        }

        [TestMethod]
        public void TestClearStack() 
        {
            // Arrange
            var stack = new MyStack<int>(3);

            // Act
            stack.Push(1);
            stack.Push(2);
            stack.Push(3);
            stack.Clear();

            // Assert
            bool expected = true;
            Assert.AreEqual(expected, stack.IsEmpty());
        }

        [TestMethod]
        public void TestContainsStack()
        {
            // Arrange
            var stack = new MyStack<int>(3);

            // Act
            stack.Push(1);
            stack.Push(2);
            stack.Push(3);

            // Assert
            bool expected = true;
            Assert.AreEqual(expected, stack.Contains(1));
        }

        [TestMethod]
        public void TestPeekStack() 
        {
            // Arrange
            var stack = new MyStack<int>(3);

            // Act
            stack.Push(1);
            stack.Push(2);
            stack.Push(3);

            // Assert
            int expected = 3;
            Assert.AreEqual(expected, stack.Peek());
        }

        [TestMethod]
        public void TestPopStackException()
        {
            // Arrange
            var stack = new MyStack<int>(3);

            // Act and Assert
            try
            {
                stack.Push(3);
                stack.Pop();
                stack.Pop();
            }
            catch (InvalidOperationException ex)
            {
                StringAssert.Contains(ex.Message, MyStack<int>.StackEmptyErrorMessage);
                return;
            }
            Assert.Fail("The expected exception was not thrown.");
        }

    }
}