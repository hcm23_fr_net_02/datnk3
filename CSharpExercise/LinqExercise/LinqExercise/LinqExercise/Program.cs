﻿using System.Text.Json;

namespace LinqExercise;

class Employee
{
    public int Id { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string? Email { get; set; }
    public string? Gender { get; set; }
    public string? Country { get; set; }
    public string? PostalCode { get; set; }
    public string? JobTitle { get; set; }
    public string? StartDate { get; set; }
    public Decimal? Salary { get; set; }
}

class JsonFileReader<T>
{
    private readonly string _fileName;
    private readonly JsonSerializerOptions _options = new()
    {
        PropertyNameCaseInsensitive = true
    };
    public JsonFileReader(string fileName)
    {
        _fileName = fileName;
    }
    public async Task<List<T>> ReadAsync()
    {
        using FileStream json = File.OpenRead(_fileName);
        List<T> list = await JsonSerializer.DeserializeAsync<List<T>>(json, _options);

        return list;
    }
}
class Program
{
    static async Task Main(string[] args)
    {
        // đọc file mock-data.json trong project để lấy dữ liệu
        var jsonReader = new JsonFileReader<Employee>(@"D:\FileExamples\mock-data.json");
        var employees = new List<Employee>();
        employees = await jsonReader.ReadAsync();

        // sử dụng LinQ để lọc các dữ liệu như yêu cầu bên dưới

        //Lấy ra danh sách các FirstName của tất cả các nhân viên.
        var firstNameList = employees.Select(x => x.FirstName);
        //foreach (var firstName in firstNameList) { Console.WriteLine(firstName); }

        //Lấy ra danh sách các nhân viên có Salary lớn hơn 50000$.
        var highSalaryList = employees.Where(e => e.Salary > 50000);
        //foreach (var employee in  highSalaryList) { Console.WriteLine(employee.FirstName); }

        //Lấy ra danh sách các nhân viên có Gender là "Male" và sắp xếp tăng dần theo FirstName.
        var maleEmployees = employees.Where(e => e.Gender == "Male").OrderBy(e => e.FirstName);

        //Lấy ra danh sách các nhân viên có Country là "Indonesia" và JobTitle chứa "Manager".
        var managerIndo = employees.Where(e => e.Country == "Indonesia" && e.JobTitle.Contains("Manager"));

        //Lấy ra danh sách các nhân viên có Email và sắp xếp giảm dần theo LastName.
        var employeeWithEmail = employees.Where(e => e.Email is not null).OrderByDescending(e => e.LastName);

        //Lấy ra danh sách các nhân viên có StartDate trước ngày "2022-01-01" và Salary lớn hơn 60000$.
        var filterEmployee = employees.Where(e => {
            var startDate = DateTime.Parse(e.StartDate);
            var compareDate = DateTime.Parse("2022-01-01");
            return startDate.CompareTo(compareDate) == -1 && e.Salary > 60000;
        });

        //Lấy ra danh sách các nhân viên có PostalCode là null hoặc rỗng và sắp xếp tăng dần theo LastName.
        var employeeWithoutPostalCode = employees.Where(e => e.PostalCode is null || e.PostalCode == "").OrderBy(e => e.LastName);

        //Lấy ra danh sách các nhân viên có FirstName và LastName được viết hoa và sắp xếp giảm dần theo Id.
        var result = employees.Where(e => char.IsUpper(e.FirstName, 0) && char.IsUpper(e.LastName, 0)).OrderByDescending(e => e.Id);

        //Lấy ra danh sách các nhân viên có Salary nằm trong khoảng từ 50000$ đến 70000$ và sắp xếp tăng dần theo Salary.
        var result1 = employees.Where(e => e.Salary >= 50000 && e.Salary <= 70000).OrderBy(e => e.Salary);

        //Lấy ra danh sách các nhân viên có FirstName chứa chữ "a" hoặc "A" và sắp xếp giảm dần theo LastName.
        var result2 = employees.Where(e => e.FirstName.Contains("a") || e.FirstName.Contains("A")).OrderByDescending(e => e.LastName);
    }
}