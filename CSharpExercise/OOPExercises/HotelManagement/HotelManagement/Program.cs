﻿
using HotelManagement.Business;
using HotelManagement.Domain;

class Program
{
    static void Main(string[] args)
    {
        var hotel = new Hotel();
        var hotelManager = new HotelManager(hotel);

        var viewController = new ViewController(hotelManager);
        viewController.Run();
    }
}