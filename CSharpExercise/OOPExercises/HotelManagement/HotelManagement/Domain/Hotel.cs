﻿namespace HotelManagement.Domain
{
    public class Hotel
    {
        public List<Room> Rooms { get; } = new List<Room>();
        public List<Booking> Bookings { get; } = new List<Booking>();

        public int GetRoomsCount()
        {
            return Rooms.Count;
        }
        public int GetBookingCount()
        {
            return Bookings.Count;
        }
    }
}
