﻿namespace HotelManagement.Domain
{
    public class Booking
    {
        public string Id { get; private set; }
        public DateTime BookDate { get; private set; }
        public DateTime CheckinDate { get; set; }
        public List<Room> BookedRooms { get; } = new List<Room>();
        public List<string> CustomerRequests { get; } = new List<string>();

        private Customer _customer;
        private Room _room;

        public Booking(Room room)
        {
            BookDate = DateTime.Now;
            _room = room;
        }
        public Booking(Customer customer, Room room)
        {
            _customer = customer;
            BookDate = DateTime.Now;
            _room = room;
        }
        public Customer Customer { get { return _customer; } }
        public Room Room { get { return _room; } }
        public override string ToString()
        {
            var requests = string.Join(", ", CustomerRequests.ToArray());
            return $"Id: {Id}; Ngày đặt: {BookDate.ToString()}; Phòng: {Room.Id}; Yêu cầu đặt biệt: {requests ?? "không có"}";
        }
    }
}
