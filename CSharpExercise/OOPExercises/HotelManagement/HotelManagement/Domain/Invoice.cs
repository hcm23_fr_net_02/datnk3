﻿using HotelManagement.RoomServices;

namespace HotelManagement.Domain
{
    public class Invoice
    {
        public string Id { get; }
        public double TotalPrice { get; set; }
        public List<IRoomService> RoomServices { get; } = new List<IRoomService>();

        private readonly Booking _booking;
        public Invoice(Booking booking)
        {
            _booking = booking;
            Id = _booking.BookDate.ToString() + _booking.Id;
        }
    }
}
