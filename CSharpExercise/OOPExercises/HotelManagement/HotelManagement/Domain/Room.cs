﻿using HotelManagement.Types;
using System.Text;

namespace HotelManagement.Domain
{
    public class Room
    {
        public string Id { get; set; }
        public RoomType Type { get; set; }
        public double Price { get; set; }
        public RoomStatus Status { get; set; }
        public List<Amenity> Amenities { get; } = new List<Amenity>();
        public override string ToString()
        {
            string amenties = string.Join(", ", Amenities.ToArray());

            return $"Id: {Id}; Loại: {Type.ToString()}; Tiện nghi: {amenties}; " +
                $"Trạng thái: {Status.ToString()}; Giá: {Price}";
        }
    }
}
