﻿using HotelManagement.Types;
using System.Text;

namespace HotelManagement.Business
{
    public class ViewController
    {
        private IManager _manager;

        private const string _MAIN_MENU = "********* Menu quản lý khách sạn *********";
        private const string _ADD_NEW_ROOM = "1. Thêm phòng mới";
        private const string _REMOVE_ROOM = "2. Xóa phòng";
        private const string _ROOM_LIST = "3. Danh sách các phòng";
        private const string _BOOKING_ROOM = "4. Đặt phòng";
        private const string _CURRENT_BOOKING = "5. Thông tin đặt phòng";
        private const string _CREATE_INVOICE = "6. Tạo hóa đơn";
        private const string _EXIT_PROGRAM = "7. Thoát chương trình";

        private string _AMENTIES_LIST = $"""
            Các loại tiện nghi:
            1. {Amenity.WIFI.ToString()}
            2. {Amenity.MINIBAR.ToString()}
            3. {Amenity.TV.ToString()}
            4. Tiếp tục
            """;
        private string _ROOMTYPE_LIST = $"""
            Các loại phòng:
            1. {RoomType.SINGLE.ToString()}
            2. {RoomType.FAMILY.ToString()}
            3. {RoomType.DOUBLE.ToString()}
            """;
        private const string _INPUT_AMENTIES = "Nhập loại tiện nghi muốn thêm";
        private const string _INPUT_ROOM_TYPE = "Nhập loại phòng";
        private const string _INPUT_PRICE = "Nhập giá phòng: ";
        private const string _INVALID_AMENITY = "Loại tiện nghi không hợp lệ";
        private const string _DUPLICATE_AMENITY = "Tiện nghi đã tồn tại";
        private const string _INVALID_ROOM_TYPE = "Loại phòng không hợp lệ";
        private const string _INVALID_PRICE = "Giá phòng không hợp lệ";

        private const string _ADD_AMENTIES_SUCCESS = "Thêm tiện nghi thành công";
        private const string _INPUT_ROOMTYPE_SUCCESS = "Chọn loại phòng thành công";
        private const string _ADD_NEW_ROOM_SUCCESS = "Thêm phòng thành công";

        private const string _INPUT_REMOVE_ROOM_ID = "Nhập mã phòng cần xóa: ";
        private const string _REMOVE_SUCCESS = "Xóa phòng thành công";
        private const string _ROOM_NOT_FOUND = "Không tìm thấy phòng";

        private const string _ROOM_LIST_MENU = "********* Xem danh sách phòng *********";
        private const string _PREVIOUS_PAGE = "1. Trang trước";
        private const string _NEXT_PAGE = "2. Trang tiếp theo";
        private const string _EXIT_TO_MAIN_MENU = "3. Quay lại menu chính";

        private string _SPECIAL_REQUESTS = "Nhập các yêu cầu đặc biệt (nhập 'exit' để dừng)";
        private string _INVALID_INPUT = "Dãy nhập không hợp lệ";

        private const string _BOOKING_SUCCESS = "Đặt phòng thành công";
        private const string _TOTAL_PRICE = "Tổng hóa đơn: ";

        private const string _BOOKING_LIST_MENU = "********* Xem danh sách phòng *********";
        public ViewController(IManager manager)
        {
            _manager = manager;

        }
        public void Run()
        {
            while (true)
            {
                ShowMainMenu();
                var input = Console.ReadLine();
                if (int.TryParse(input, out int choice))
                {
                    switch (choice)
                    {
                        case 1:
                            HandleAddNewRoom();
                            break;
                        case 2:
                            HandleRemoveRoom();
                            break;
                        case 3:
                            HandleShowRoomList();
                            break;
                        case 4:
                            HandleBookingRoom();
                            break;
                        case 5:
                            HandleShowBookingInfo();
                            break;
                        case 6:
                            HandleCreateInvoice();
                            break;
                        case 7:
                            break;
                    }
                }
                if (choice == 7) break;
            }
        }
        private void ShowMainMenu()
        {
            Console.Clear();
            Console.OutputEncoding = Encoding.UTF8;
            _MAIN_MENU.PrintLine();
            _ADD_NEW_ROOM.PrintLine();
            _REMOVE_ROOM.PrintLine();
            _ROOM_LIST.PrintLine();
            _BOOKING_ROOM.PrintLine();
            _CURRENT_BOOKING.PrintLine();
            _CREATE_INVOICE.PrintLine();
            _EXIT_PROGRAM.PrintLine();
        }
        private void HandleAddNewRoom()
        {
            Console.Clear();
            Console.OutputEncoding = Encoding.UTF8;

            List<Amenity> amenities = new();
            RoomType roomType;
            double price;

            _AMENTIES_LIST.PrintLine();
            Console.WriteLine();
            
            while (true)
            {
                _INPUT_AMENTIES.PrintLine();
                var input = Console.ReadKey(true).Key;
                if (input == ConsoleKey.D1 || input == ConsoleKey.D2 || input == ConsoleKey.D3)
                {
                    if (amenities.Contains((Amenity)input))
                    {
                        _DUPLICATE_AMENITY.PrintError();
                    }
                    else
                    {
                        amenities.Add((Amenity)(input-49));
                        _ADD_AMENTIES_SUCCESS.PrintSuccess();
                    }
                }
                else if (input == ConsoleKey.D4)
                {
                    break;
                }
                else
                {
                    _INVALID_AMENITY.PrintError();
                }
            }

            Console.Clear();
            _ROOMTYPE_LIST.PrintLine();
            Console.WriteLine();

            while (true)
            {
                _INPUT_ROOM_TYPE.PrintLine();
                var input = Console.ReadKey(true).Key;
                if (input == ConsoleKey.D1 || input == ConsoleKey.D2 || input == ConsoleKey.D3)
                {
                    roomType = (RoomType)(input-49);
                    _INPUT_ROOMTYPE_SUCCESS.PrintSuccess();
                    Console.ReadLine();
                    break;
                }
                else
                {
                    _INVALID_ROOM_TYPE.PrintError();
                }
            }

            Console.Clear();
            do
            {
                _INPUT_PRICE.Print();

                if (double.TryParse(Console.ReadLine(), out double _price) && _price > 0)
                {
                    price = _price;
                    break;
                }
                else
                {
                    _INVALID_PRICE.PrintError();
                }
            } while (true);

            Console.Clear();
            _manager.AddRoom(amenities, roomType, price);
            _ADD_NEW_ROOM_SUCCESS.PrintSuccess();
            Console.ReadLine();
        }
        private void HandleRemoveRoom()
        {
            Console.Clear();
            Console.OutputEncoding = Encoding.UTF8;
            _INPUT_REMOVE_ROOM_ID.Print();
            string id = Console.ReadLine();
            bool isRemoved = _manager.RemoveRoom(id);

            if (isRemoved)
            {
                _REMOVE_SUCCESS.PrintSuccess();
            }
            else
            {
                _ROOM_NOT_FOUND.PrintError();
            }
            Console.ReadLine();
        }
        private void HandleShowRoomList()
        {
            double totalPage = Math.Ceiling(_manager.GetRooms().Count * 1.0 / 5);
            int currentPage = 1;

            ShowRoomList(currentPage);
            while (true)
            {
                var input = Console.ReadKey(true).Key;

                if (input == ConsoleKey.D1 && currentPage > 1)
                {
                    currentPage--;
                    ShowRoomList(currentPage);
                }
                else if (input == ConsoleKey.D2 && currentPage < totalPage)
                {
                    currentPage++;
                    ShowRoomList(currentPage);
                }
                else if (input == ConsoleKey.D3)
                {
                    break;
                }
            }
        }
        private void ShowRoomList(int page)
        {
            Console.Clear();
            var rooms = _manager.GetRoomsPaging(page - 1);
            for (int i = 0; i < 5; i++)
            {
                if (i < rooms.Count)
                {
                    Console.WriteLine(rooms[i]);
                }
                else
                {
                    Console.WriteLine();
                }
            }

            Console.WriteLine();
            Console.OutputEncoding = Encoding.UTF8;
            _ROOM_LIST_MENU.PrintLine();
            _PREVIOUS_PAGE.PrintLine();
            _NEXT_PAGE.PrintLine();
            _EXIT_TO_MAIN_MENU.PrintLine();
        }
        private void HandleBookingRoom()
        {
            Console.Clear();
            RoomType roomType;
            List<string> requests = new List<string>();

            _ROOMTYPE_LIST.PrintLine();
            Console.WriteLine();

            while (true)
            {
                _INPUT_ROOM_TYPE.PrintLine();
                var input = Console.ReadKey(true).Key;
                if (input == ConsoleKey.D1 || input == ConsoleKey.D2 || input == ConsoleKey.D3)
                {
                    roomType = (RoomType)(input - 49);
                    _INPUT_ROOMTYPE_SUCCESS.PrintSuccess();
                    Console.ReadLine();
                    break;
                }
                else
                {
                    _INVALID_ROOM_TYPE.PrintError();
                }
            }

            Console.Clear();

            _SPECIAL_REQUESTS.PrintLine();
            int i = 1;
            while (true)
            {
                Console.Write($"{i}. ");
                var input = Console.ReadLine();

                if (input is null)
                {
                    _INVALID_INPUT.PrintError();
                }
                else if (input == "exit")
                {
                    break;
                }
                else
                {
                    i++;
                    requests.Add(input);
                }
            }

            Console.Clear();

            try
            {
                var booking = _manager.CreateBooking(roomType, requests);
                _BOOKING_SUCCESS.PrintSuccess();
                _TOTAL_PRICE.Print();
                Console.Write(booking.Room.Price);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            Console.ReadLine();
        }
        private void HandleShowBookingInfo()
        {
            double totalPage = Math.Ceiling(_manager.GetBookings().Count * 1.0 / 5);
            int currentPage = 1;

            ShowBookingInfo(currentPage);
            while (true)
            {
                var input = Console.ReadKey(true).Key;

                if (input == ConsoleKey.D1 && currentPage > 1)
                {
                    currentPage--;
                    ShowBookingInfo(currentPage);
                }
                else if (input == ConsoleKey.D2 && currentPage < totalPage)
                {
                    currentPage++;
                    ShowBookingInfo(currentPage);
                }
                else if (input == ConsoleKey.D3)
                {
                    break;
                }
            }
        }
        private void ShowBookingInfo(int page)
        {
            Console.Clear();
            var bookings = _manager.GetBookingsPaging(page - 1);
            for (int i = 0; i < 5; i++)
            {
                if (i < bookings.Count)
                {
                    Console.WriteLine(bookings[i]);
                }
                else
                {
                    Console.WriteLine();
                }
            }

            Console.WriteLine();
            Console.OutputEncoding = Encoding.UTF8;
            _BOOKING_LIST_MENU.PrintLine();
            _PREVIOUS_PAGE.PrintLine();
            _NEXT_PAGE.PrintLine();
            _EXIT_TO_MAIN_MENU.PrintLine();
        }
        private void HandleCreateInvoice()
        {

        }

    }
}
