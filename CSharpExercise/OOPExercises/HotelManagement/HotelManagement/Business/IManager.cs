﻿using HotelManagement.Domain;
using HotelManagement.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HotelManagement.Business
{
    public interface IManager
    {
        void AddRoom(List<Amenity> amenities, RoomType type, double price);
        bool RemoveRoom(string id);
        List<Room> GetRooms();
        List<Room> GetRoomsPaging(int pageIndex = 0, int elementPerPage = 5);
        Booking CreateBooking(RoomType type, List<string> requests, Customer? customer = null);
        List<Booking> GetBookings();
        List<Booking> GetBookingsPaging(int pageIndex = 0, int elementPerPage = 5); 
        Invoice CreateInvoice(string customerName);
    }
}
