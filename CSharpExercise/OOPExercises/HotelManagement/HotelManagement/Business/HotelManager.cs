﻿using HotelManagement.Commons;
using HotelManagement.Domain;
using HotelManagement.Types;

namespace HotelManagement.Business
{
    public class HotelManager : IManager
    {
        private readonly Hotel _hotel;
        public HotelManager(Hotel hotel)
        {
            _hotel = hotel;
        }
        public void AddRoom(List<Amenity> amenities, RoomType type, double price)
        {
            var newRoom = new Room();
            newRoom.Id = _hotel.GetRoomsCount().ToString();
            newRoom.Status = RoomStatus.EMPTY;
            newRoom.Price = price;
            newRoom.Type = type;
            foreach (var amenty in amenities) { newRoom.Amenities.Add(amenty); }
            _hotel.Rooms.Add(newRoom);
        }
        public bool RemoveRoom(string id)
        {
            var index = _hotel.Rooms.FindIndex(room => room.Id == id);
            if (index >= 0)
            {
                _hotel.Rooms.RemoveAt(index);
                return true;
            }
            return false;
        }
        public List<Room> GetRooms()
        {
            return _hotel.Rooms;
        }
        public Booking CreateBooking(RoomType type, List<string> requests, Customer? customer = null)
        {
            var rooms = _hotel.Rooms.Find(r => r.Type == type);
            if (rooms is null) 
            {
                throw new BookingException("Không tìm thấy phòng phù hợp");
            } 

            Booking newBooking;
            newBooking = customer is null ? new Booking(rooms) : new Booking(customer, rooms);
            foreach(var request in requests)
            {
                newBooking.CustomerRequests.Add(request);
            }
            _hotel.Bookings.Add(newBooking);

            return newBooking;
        }
        public List<Booking> GetBookings()
        {
            return _hotel.Bookings;
        }
        public Invoice CreateInvoice(string customerName)
        {
            throw new NotImplementedException();
        }

        public List<Room> GetRoomsPaging(int pageIndex = 0, int elementPerPage = 5)
        {
            return _hotel.Rooms.Skip(pageIndex * elementPerPage).Take(elementPerPage).ToList();
        }

        public List<Booking> GetBookingsPaging(int pageIndex = 0, int elementPerPage = 5)
        {
            return _hotel.Bookings.Skip(pageIndex * elementPerPage).Take(elementPerPage).ToList();
        }
    }
}
