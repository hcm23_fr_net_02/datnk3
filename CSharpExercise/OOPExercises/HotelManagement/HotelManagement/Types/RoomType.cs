﻿namespace HotelManagement.Types
{
    public enum RoomType
    {
        SINGLE,
        DOUBLE,
        FAMILY
    }
}
