﻿namespace HotelManagement.Types
{
    public enum Amenity
    {
        WIFI,
        TV,
        MINIBAR
    }
}
