﻿namespace HotelManagement.RoomServices
{
    public class CleanService : IRoomService
    {
        public double GetPrice()
        {
            return 50.0;
        }

        public string GetServiceName()
        {
            return "Clean Service";
        }
    }
}
