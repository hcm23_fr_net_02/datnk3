﻿namespace HotelManagement.RoomServices
{
    public interface IRoomService
    {
        string GetServiceName();
        double GetPrice();
    }
}
