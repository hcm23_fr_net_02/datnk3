﻿namespace HotelManagement.RoomServices
{
    public class DinnerService : IRoomService
    {
        public double GetPrice()
        {
            return 70.0;
        }

        public string GetServiceName()
        {
            return "Dinner Service";
        }
    }
}
