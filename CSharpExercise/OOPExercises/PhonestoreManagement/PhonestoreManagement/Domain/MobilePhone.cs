﻿using PhonestoreManagement.Types;

namespace PhonestoreManagement.Domain
{
    public class MobilePhone
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public Brand Brand { get; set; }
        public static int InStock { get; set; } = 0;
        public double Price { get; set; }
        public string Code { get; set; }
        public override string ToString()
        {
            return $"Id: {Id}; Tên: {Name}; Hãng: {Brand.ToString()}; Giá: {Price}; Mã: {Code}";
        }
    }
}
