﻿namespace PhonestoreManagement.Domain
{
    public class Store
    {
        private int _phoneId = 0;
        private int _purchaseId = 0;
        public List<MobilePhone> Phones { get; set; } = new List<MobilePhone>();
        public List<Purchase> Purchases { get; set; } = new List<Purchase>();
        public int GetPhoneId()
        {
            return _phoneId++;
        }
        public int GetPurchaseId()
        {
            return _purchaseId++;
        }
    }
}
