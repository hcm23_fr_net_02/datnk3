﻿namespace PhonestoreManagement.Domain
{
    public class Purchase
    {
        public string Id { get; set; }
        public DateTime PurchaseDate { get; } = DateTime.Now;
        public List<MobilePhone> Phones { get; set; } = new List<MobilePhone>();
        public Customer Customer { get; }
        public Purchase(Customer customer)
        {
            Customer = customer;
        }
        public override string ToString()
        {
            List<string> phoneNames = new List<string>();
            foreach (MobilePhone phone in Phones)
            {
                phoneNames.Add(phone.Name);
            }
            
            var phones = string.Join(", ", phoneNames);
            return $"Id: {Id}; Ngày mua: {PurchaseDate}; Khách hàng: {Customer.Name}, Sản phẩm: {phones}";
        }
    }
}
