﻿using PhonestoreManagement.Commons;
using PhonestoreManagement.Types;
using System.Text;

namespace PhonestoreManagement.Business
{
    public class ViewController
    {
        private IManager _manager;

        private const string _MAIN_MENU = "********* Menu quản lý cửa hàng điện thoại *********";
        private const string _ADD_NEW_PHONE = "1. Thêm điện thoại";
        private const string _REMOVE_PHONE = "2. Xóa điện thoại";
        private const string _PHONE_LIST = "3. Danh sách các điện thoại";
        private const string _PURCHASE_PHONE = "4. Mua điện thoại";
        private const string _PURCHASE_LIST = "5. Thông tin mua hàng";
        private const string _FIND_PHONE = "6. Tìm điện thoại";
        private const string _EXIT_PROGRAM = "7. Thoát chương trình";

        private string _BRAND_LIST = $"""
            Các hãng điện thoại:
            1. {Brand.Apple.ToString()}
            2. {Brand.Samsung.ToString()}
            3. {Brand.Sony.ToString()}
            4. {Brand.Google.ToString()}
            5. {Brand.Nokia.ToString()}
            """;

        private const string _INPUT_NAME = "Tên điện thoại: ";
        private const string _INPUT_BRAND = "Hãng: ";
        private const string _INPUT_PRICE = "Giá: ";
        private const string _INPUT_PRODUCT_CODE = "Nhập Mã sản phẩm: ";

        private const string _PHONE_LIST_MENU = "********* Menu Xem danh sách điện thoại *********";
        private const string _PURCHASE_LIST_MENU = "********* Menu Xem danh sách điện thoại *********";
        private const string _PREVIOUS_PAGE = "1. Trang trước";
        private const string _NEXT_PAGE = "2. Trang tiếp theo";
        private const string _EXIT_TO_MAIN_MENU = "3. Quay lại menu chính";

        private const string _INVALID_NAME = "Input không hợp lệ, xin mời nhập lại";
        private const string _INVALID_BRAND_TYPE = "Hãng không hợp lệ";
        private const string _INVALID_PRICE = "Giá không hợp lệ";
        private const string _INVALID_PRODUCT_CODE = "Nhập Mã sản phẩm: ";

        private const string _INPUT_BRAND_SUCCESS = "Chọn hãng thành công";
        private const string _ADD_PHONE_SUCCESS = "Thêm điện thoại thành công";

        private const string _INPUT_REMOVE_PHONE_ID = "Nhập mã điện thoại cần xóa: ";
        private const string _REMOVE_SUCCESS = "Xóa điện thoại thành công";
        private const string _PHONE_NOT_FOUND = "Không tìm thấy điện thoại";

        public ViewController(IManager manager) 
        {
            _manager = manager;
        }
        public void Run()
        {
            while (true)
            {
                ShowMainMenu();
                var input = Console.ReadLine();
                if (int.TryParse(input, out int choice))
                {
                    switch (choice)
                    {
                        case 1:
                            HandleAddNewPhone();
                            break;
                        case 2:
                            HandleRemovePhone();
                            break;
                        case 3:
                            HandleShowPhoneList();
                            break;
                        case 4:
                            HandlePurchasePhone();
                            break;
                        case 5:
                            HandleShowPurchaseInfo();
                            break;
                        case 6:
                            HandleFindPhone();
                            break;
                        case 7:
                            break;
                    }
                }
                if (choice == 7) break;
            }
        }
        public void ShowMainMenu()
        {
            Console.Clear();
            Console.OutputEncoding = Encoding.UTF8;
            _MAIN_MENU.PrintLine();
            _ADD_NEW_PHONE.PrintLine();
            _REMOVE_PHONE.PrintLine();
            _PHONE_LIST.PrintLine();
            _PURCHASE_PHONE.PrintLine();
            _PURCHASE_LIST.PrintLine();
            _FIND_PHONE.PrintLine();
            _EXIT_PROGRAM.PrintLine();
        }
        private void HandleFindPhone()
        {
            Console.Clear();
            Console.OutputEncoding = Encoding.UTF8;
            _INPUT_PRODUCT_CODE.Print();
            string value = Console.ReadLine();
            var foundPhone = _manager.GetPhone(value);

            if (foundPhone is null)
            {
                _PHONE_NOT_FOUND.PrintError();
            }
            else
            {
                Console.WriteLine(foundPhone);
            }
            Console.ReadLine();
        }

        private void HandleShowPurchaseInfo()
        {
            throw new NotImplementedException();
        }

        private void HandlePurchasePhone()
        {
            throw new NotImplementedException();
        }

        private void HandleShowPhoneList()
        {
            double totalPage = Math.Ceiling(_manager.GetPhones().Count * 1.0 / 5);
            int currentPage = 1;

            ShowPhoneList(currentPage);
            while (true)
            {
                var input = Console.ReadKey(true).Key;

                if (input == ConsoleKey.D1 && currentPage > 1)
                {
                    currentPage--;
                    ShowPhoneList(currentPage);
                }
                else if (input == ConsoleKey.D2 && currentPage < totalPage)
                {
                    currentPage++;
                    ShowPhoneList(currentPage);
                }
                else if (input == ConsoleKey.D3)
                {
                    break;
                }
            }
        }
        private void ShowPhoneList(int page)
        {
            Console.Clear();
            var phones = _manager.GetPhonesPaging(page - 1);
            for (int i = 0; i < 5; i++)
            {
                if (i < phones.Count)
                {
                    Console.WriteLine(phones[i]);
                }
                else
                {
                    Console.WriteLine();
                }
            }

            Console.WriteLine();
            Console.OutputEncoding = Encoding.UTF8;
            _PHONE_LIST_MENU.PrintLine();
            _PREVIOUS_PAGE.PrintLine();
            _NEXT_PAGE.PrintLine();
            _EXIT_TO_MAIN_MENU.PrintLine();
        }

        private void HandleRemovePhone()
        {
            Console.Clear();
            Console.OutputEncoding = Encoding.UTF8;
            _INPUT_REMOVE_PHONE_ID.Print();
            string id = Console.ReadLine();
            bool isRemoved = _manager.RemovePhone(id);

            if (isRemoved)
            {
                _REMOVE_SUCCESS.PrintSuccess();
            }
            else
            {
                _PHONE_NOT_FOUND.PrintError();
            }
            Console.ReadLine();
        }

        private void HandleAddNewPhone()
        {
            Console.Clear();
            Console.OutputEncoding = Encoding.UTF8;

            string name;
            Brand brand;
            double price;
            string code;

            do
            {
                _INPUT_NAME.Print();
                name = Console.ReadLine();
                if (name is null)
                {
                    _INVALID_NAME.PrintError();
                }
            } while (name is null);

            Console.Clear();
            _BRAND_LIST.PrintLine();
            Console.WriteLine();
            while (true)
            {
                _INPUT_BRAND.Print();
                var input = Console.ReadKey(true).Key;
                if (input == ConsoleKey.D1 || 
                    input == ConsoleKey.D2 || 
                    input == ConsoleKey.D3 ||
                    input == ConsoleKey.D4 ||
                    input == ConsoleKey.D5
                    )
                {
                    brand = (Brand)(input - 49);
                    _INPUT_BRAND_SUCCESS.PrintSuccess();
                    Console.ReadLine();
                    break;
                }
                else
                {
                    _INVALID_BRAND_TYPE.PrintError();
                }
            }

            Console.Clear();
            do
            {
                _INPUT_PRICE.Print();

                if (double.TryParse(Console.ReadLine(), out double _price) && _price > 0)
                {
                    price = _price;
                    break;
                }
                else
                {
                    _INVALID_PRICE.PrintError();
                }
            } while (true);

            Console.Clear();
            do
            {
                _INPUT_PRODUCT_CODE.Print();
                code = Console.ReadLine();
                if (code is null)
                {
                    _INVALID_PRODUCT_CODE.PrintError();
                }
            } while (name is null);

            Console.Clear();
            _manager.AddNewPhone(name, brand, price, code);
            _ADD_PHONE_SUCCESS.PrintSuccess();
            Console.ReadLine();
        }
    }
}
