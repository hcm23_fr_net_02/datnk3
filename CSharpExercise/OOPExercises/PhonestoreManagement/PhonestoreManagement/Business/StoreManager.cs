﻿using PhonestoreManagement.Domain;
using PhonestoreManagement.Types;

namespace PhonestoreManagement.Business
{
    public class StoreManager : IManager
    {
        private Store _store;
        public StoreManager(Store store)
        {
            _store = store;
        }
        public void AddNewPhone(string name, Brand brand, double price, string code)
        {
            var phone = new MobilePhone();
            phone.Id = _store.GetPhoneId().ToString();
            phone.Name = name;
            phone.Brand = brand;
            phone.Price = price;
            phone.Code = code;
            MobilePhone.InStock++;
            _store.Phones.Add(phone);
        }

        public Purchase CreatePurchase(Customer customer, List<MobilePhone> phones)
        {
            var purchase = new Purchase(customer);
            purchase.Phones = phones;
            return purchase;
        }

        public MobilePhone? GetPhone(string code)
        {
            return _store.Phones.Find(x => x.Code == code);
        }

        public List<MobilePhone> GetPhones()
        {
            return _store.Phones;
        }

        public List<MobilePhone> GetPhonesPaging(int pageIndex = 0, int elementPerPage = 5)
        {
            return _store.Phones.Skip(pageIndex * elementPerPage).Take(elementPerPage).ToList();
        }

        public List<Purchase> GetPurchases()
        {
            return _store.Purchases;
        }

        public List<Purchase> GetPurchasesPaging(int pageIndex = 0, int elementPerPage = 5)
        {
            return _store.Purchases.Skip(pageIndex * elementPerPage).Take(elementPerPage).ToList();
        }

        public bool RemovePhone(string id)
        {
            var index = _store.Phones.FindIndex(phone => phone.Id == id);
            if (index >= 0)
            {
                _store.Phones.RemoveAt(index);
                return true;
            }
            return false;
        }
    }
}
