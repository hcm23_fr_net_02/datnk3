﻿using PhonestoreManagement.Domain;
using PhonestoreManagement.Types;

namespace PhonestoreManagement.Business
{
    public interface IManager
    {
        void AddNewPhone(string name, Brand brand, double price, string code);
        bool RemovePhone(string id);
        List<MobilePhone> GetPhones();
        List<MobilePhone> GetPhonesPaging(int pageIndex = 0, int elementPerPage = 5);
        Purchase CreatePurchase(Customer customer, List<MobilePhone> phones);
        List<Purchase> GetPurchases();
        List<Purchase> GetPurchasesPaging(int pageIndex = 0, int elementPerPage = 5);
        MobilePhone? GetPhone(string code);
    }
}
