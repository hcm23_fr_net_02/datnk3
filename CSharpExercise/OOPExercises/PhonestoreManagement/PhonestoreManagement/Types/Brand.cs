﻿namespace PhonestoreManagement.Types
{
    public enum Brand
    {
        Samsung,
        Apple,
        Nokia,
        Sony,
        Google
    }
}
