﻿using PhonestoreManagement.Business;
using PhonestoreManagement.Domain;

namespace PhonestoreManagement;

class Program
{
    static void Main(string[] args)
    {
        var store = new Store();
        var storeManager = new StoreManager(store);
        var viewController = new ViewController(storeManager);
        viewController.Run();
    }
}