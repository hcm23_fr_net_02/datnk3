﻿namespace PhonestoreManagement.Commons
{
    public static class MyExtensions
    {
        public static void PrintLine(this string input)
        {
            Console.WriteLine(input);
        }
        public static void Print(this string input)
        {
            Console.Write(input);
        }
        public static void PrintError(this string input)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(input);
            Console.ResetColor();
        }
        public static void PrintSuccess(this string input)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(input);
            Console.ResetColor();
        }
    }
}
