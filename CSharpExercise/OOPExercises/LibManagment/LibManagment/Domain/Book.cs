﻿namespace LibManagment.Domain
{
    public class Book
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public List<string> Authors { get; set; }
        public static int InStock { get; set; }
        public double Price { get; set; }
        public string Code { get; set; }
        public override string ToString()
        {
            var authors = string.Join(", ", Authors.ToArray());
            return $"Id: {Id}; Tiêu đề: {Title}; Tác giả: {authors}; Giá: {Price}; Mã số sách: {Code}";
        }
    }
}
