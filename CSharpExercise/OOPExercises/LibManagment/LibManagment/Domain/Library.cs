﻿namespace LibManagment.Domain
{
    public class Library
    {
        private int _bookId = 0;
        private int _loanId = 0;
        private int _customerId = 0;
        public List<Book> Books { get; set; } = new List<Book>();
        public List<Loan> Loans { get; set; } = new List<Loan>();
        public List<Customer> Customer { get; set; } = new List<Customer>();
        public int GetBookId()
        {
            return _bookId++;
        }
        public int GetLoanId()
        {
            return _loanId++;
        }
        public int GetCustomerId()
        {
            return _customerId++;
        }
    }
}
