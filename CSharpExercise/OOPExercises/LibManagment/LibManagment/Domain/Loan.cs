﻿namespace LibManagment.Domain
{
    public class Loan
    {
        public string Id { get; set; }
        public DateTime LoanDate { get; } = DateTime.Now;
        public DateTime EndDate { get; set; }
        public List<Book> Books { get; set; }
        public Customer Customer { get; private set; }
        public Loan(Customer customer)
        {
            Customer = customer;
        }
        public override string ToString()
        {
            List<string> bookNames = new();
            foreach (var book in Books)
            {
                bookNames.Add(book.Title);
            }
            var loanBooks = string.Join(", ", bookNames.ToArray());
            return $"Id: {Id}; Ngày mượn: {LoanDate}; Ngày trả: {EndDate}; Khách: {Customer.Name}; Sách: {loanBooks}";
        }
    }
}
