﻿
using LibManagment.Business;
using LibManagment.Domain;

class Program
{
    static void Main(string[] args)
    {
        var library = new Library();
        var libraryManager = new LibraryManager(library);
        var viewController = new ViewController(libraryManager);
        viewController.Run();
    }
}