﻿using LibManagment.Domain;

namespace LibManagment.Business
{
    public class LibraryManager : IManager
    {
        private Library _library;
        public LibraryManager(Library library)
        {
            _library = library;
        }

        public void AddNewBook(string title, List<string> authors, double price, string code)
        {
            var newBook = new Book();
            newBook.Id = _library.GetBookId().ToString();
            newBook.Title = title;
            newBook.Authors = authors;
            newBook.Price = price;
            newBook.Code = code;
            Book.InStock++;
            _library.Books.Add(newBook);
        }

        public Loan CreateLoan(Customer customer, int loanDay, List<Book> books)
        {
            var loan = new Loan(customer);
            loan.Id = _library.GetLoanId().ToString();
            loan.Books = books;
            loan.EndDate = DateTime.Now.AddDays(loanDay);
            _library.Loans.Add(loan);
            return loan;
        }
        public Customer CreateCustomer(string name, string address, string phone)
        {
            var customer = new Customer();
            customer.Id = _library.GetCustomerId().ToString();
            customer.Name = name;
            customer.Address = address;
            customer.Phone = phone;
            customer.MemberCode = name + phone.Substring(phone.Length - 3);
            _library.Customer.Add(customer);
            return customer;
        }
        public Customer? FindCustomer(string value)
        {
            return _library.Customer.Find(c => c.Id == value || c.MemberCode == value);
        }
        public Book? GetBook(string code)
        {
            return _library.Books.Find(b => b.Code == code);
        }

        public List<Book> GetBooks()
        {
            return _library.Books;
        }

        public List<Book> GetBooksPaging(int pageIndex = 0, int elementPerPage = 5)
        {
            return _library.Books.Skip(pageIndex * elementPerPage).Take(elementPerPage).ToList();
        }

        public List<Loan> GetLoans()
        {
            return _library.Loans;
        }

        public List<Loan> GetLoansPaging(int pageIndex = 0, int elementPerPage = 5)
        {
            return _library.Loans.Skip(pageIndex * elementPerPage).Take(elementPerPage).ToList();
        }

        public bool RemoveBook(string id)
        {
            var index = _library.Books.FindIndex(book => book.Id == id);
            if (index >= 0)
            {
                _library.Books.RemoveAt(index);
                return true;
            }
            return false;
        }
    }
}
