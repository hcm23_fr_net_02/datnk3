﻿using LibManagment.Commons;
using LibManagment.Domain;
using System.Diagnostics;
using System.Text;
using System.Xml.Linq;

namespace LibManagment.Business
{
    public class ViewController
    {
        private IManager _manager;

        private const string _MAIN_MENU = "********* Menu quản lý thư viện *********";
        private const string _ADD_NEW_BOOK   = "1. Thêm sách mới";
        private const string _REMOVE_BOOK    = "2. Xóa sách";
        private const string _BOOK_LIST      = "3. Danh sách các cuốn sách";
        private const string _LOAN_BOOK      = "4. Mượn sách";
        private const string _LOAN_INFO      = "5. Thông tin mượn sách";
        private const string _FIND_BOOK      = "6. Tìm sách";
        private const string _EXIT_PROGRAM   = "7. Thoát chương trình";

        private const string _INPUT_TITLE = "Tên sách: ";
        private const string _INPUT_AUTHORS = "Nhập tên tác giả (nhập 'exit' để tiếp tục)";
        private const string _INPUT_PRICE = "Giá: ";
        private const string _INPUT_BOOK_CODE = "Nhập Mã cuốn sách: ";

        private const string _INVALID_TITLE = "Input không hợp lệ, xin mời nhập lại";
        private const string _INVALID_AUTHOR_NAME = "Tên tác giả không hợp lệ";
        private const string _INVALID_PRICE = "Giá không hợp lệ";
        private const string _INVALID_BOOK_CODE = "Mã sách không hợp lệ";

        private const string _ADD_NEW_BOOK_SUCCESS = "Thêm sách thành công";

        private const string _INPUT_REMOVE_BOOK_ID = "Nhập id sách cần xóa: ";
        private const string _REMOVE_SUCCESS = "Xóa phòng thành công";
        private const string _BOOK_NOT_FOUND = "Không tìm thấy sách";

        private const string _BOOK_LIST_MENU = "********* Xem danh sách các cuốn sách *********";
        private const string _LOAN_LIST_MENU = "********* Xem thông tin mượn sách *********";
        private const string _PREVIOUS_PAGE = "1. Trang trước";
        private const string _NEXT_PAGE = "2. Trang tiếp theo";
        private const string _EXIT_TO_MAIN_MENU = "3. Quay lại menu chính";

        private const string _CHECK_MEMBER_EXIST = "Đã có mã thành viên? (Y/N) ";
        private const string _INPUT_MEMBER_CODE = "Nhập mã thành viên (tên + 3 số cuối điện thoại): ";
        private const string _MEMBER_NOT_FOUND = "Không tìm thấy thành viên";
        private const string _INPUT_LOAN_BOOKS = "Nhập mã sách mượn (nhập 'exit' để tiếp tục)";
        private const string _INPUT_LOAN_DAY = "Nhập số ngày mượn: ";
        private const string _CREATE_LOAN_SUCCESS = "Đăng ký mượn sách thành công";
        private const string _CREATE_NEW_CUSTOMER = "********* Đăng ký thành viên mới *********";
        private const string _INPUT_CUSTOMER_NAME = "Nhập họ và tên: ";
        private const string _INPUT_CUSTOMER_ADDRESS = "Nhập địa chỉ: ";
        private const string _INPUT_CUSTOMER_PHONE = "Nhập số điện thoại: ";
        private const string _CREATE_CUSTOMER_SUCCESS = "Đăng ký thành công";
        private const string _INVALID_NAME = "Tên không hợp lệ";
        private const string _INVALID_ADDRESS = "Địa chỉ không hợp lệ";
        private const string _INVALID_PHONE = "Số điện thoại không hợp lệ";
        private const string _INVALID_DAY = "Số ngày không hợp lệ";

        public ViewController(IManager manager)
        {
            _manager = manager;
        }
        public void Run()
        {
            while (true)
            {
                ShowMainMenu();
                var input = Console.ReadLine();
                if (int.TryParse(input, out int choice))
                {
                    switch (choice)
                    {
                        case 1:
                            HandleAddNewBook();
                            break;
                        case 2:
                            HandleRemoveBook();
                            break;
                        case 3:
                            HandleShowBookList();
                            break;
                        case 4:
                            HandleLoanBook();
                            break;
                        case 5:
                            HandleShowLoanInfo();
                            break;
                        case 6:
                            HandleFindBook();
                            break;
                        case 7:
                            break;
                    }
                }
                if (choice == 7) break;
            }
        }

        private void ShowMainMenu()
        {
            Console.Clear();
            Console.OutputEncoding = Encoding.UTF8;
            _MAIN_MENU.PrintLine();
            _ADD_NEW_BOOK.PrintLine();
            _REMOVE_BOOK .PrintLine();
            _BOOK_LIST   .PrintLine();
            _LOAN_BOOK   .PrintLine();
            _LOAN_INFO   .PrintLine();
            _FIND_BOOK   .PrintLine();
            _EXIT_PROGRAM.PrintLine();
        }

        private void HandleAddNewBook()
        {
            Console.Clear();
            Console.OutputEncoding = Encoding.UTF8;

            string title;
            List<string> authors = new List<string>();
            double price;
            string code;

            do
            {
                _INPUT_TITLE.Print();
                title = Console.ReadLine();
                if (title is null)
                {
                    _INVALID_TITLE.PrintError();
                }
            } while (title is null);

            Console.Clear();
            _INPUT_AUTHORS.PrintLine();
            Console.WriteLine();
            int i = 1;
            while (true)
            {
                Console.Write($"{i}. ");
                var input = Console.ReadLine();

                if (input is null)
                {
                    _INVALID_AUTHOR_NAME.PrintError();
                }
                else if (input == "exit")
                {
                    break;
                }
                else
                {
                    i++;
                    authors.Add(input);
                }
            }

            Console.Clear();
            do
            {
                _INPUT_PRICE.Print();

                if (double.TryParse(Console.ReadLine(), out double _price) && _price > 0)
                {
                    price = _price;
                    break;
                }
                else
                {
                    _INVALID_PRICE.PrintError();
                }
            } while (true);

            Console.Clear();
            do
            {
                _INPUT_BOOK_CODE.Print();
                code = Console.ReadLine();
                if (code is null)
                {
                    _INVALID_BOOK_CODE.PrintError();
                }
            } while (code is null);

            Console.Clear();
            _ADD_NEW_BOOK_SUCCESS.PrintSuccess();
            _manager.AddNewBook(title, authors, price, code);
            Console.ReadLine();
        }

        private void HandleRemoveBook()
        {
            Console.Clear();
            Console.OutputEncoding = Encoding.UTF8;
            _INPUT_REMOVE_BOOK_ID.Print();
            string id = Console.ReadLine();
            bool isRemoved = _manager.RemoveBook(id);

            if (isRemoved)
            {
                _REMOVE_SUCCESS.PrintSuccess();
            }
            else
            {
                _BOOK_NOT_FOUND.PrintError();
            }
            Console.ReadLine();
        }

        private void HandleShowBookList()
        {
            double totalPage = Math.Ceiling(_manager.GetBooks().Count * 1.0 / 5);
            int currentPage = 1;

            ShowBookList(currentPage);
            while (true)
            {
                var input = Console.ReadKey(true).Key;

                if (input == ConsoleKey.D1 && currentPage > 1)
                {
                    currentPage--;
                    ShowBookList(currentPage);
                }
                else if (input == ConsoleKey.D2 && currentPage < totalPage)
                {
                    currentPage++;
                    ShowBookList(currentPage);
                }
                else if (input == ConsoleKey.D3)
                {
                    break;
                }
            }
        }
        private void ShowBookList(int page)
        {
            Console.Clear();
            var books = _manager.GetBooksPaging(page - 1);
            for (int i = 0; i < 5; i++)
            {
                if (i < books.Count)
                {
                    Console.WriteLine(books[i]);
                }
                else
                {
                    Console.WriteLine();
                }
            }

            Console.WriteLine();
            Console.OutputEncoding = Encoding.UTF8;
            _BOOK_LIST_MENU.PrintLine();
            _PREVIOUS_PAGE.PrintLine();
            _NEXT_PAGE.PrintLine();
            _EXIT_TO_MAIN_MENU.PrintLine();
        }
        private void HandleLoanBook()
        {
            Console.Clear();
            _CHECK_MEMBER_EXIST.Print();

            Customer customer;
            int loanDay;
            string memberCode;
            List<Book> books = new();
            while (true)
            {
                var input = Console.ReadKey(true).Key;

                if (input == ConsoleKey.Y)
                {
                    Console.Clear();
                    _INPUT_MEMBER_CODE.Print();
                    memberCode = Console.ReadLine();
                    customer = _manager.FindCustomer(memberCode);
                    if (customer is not null)
                    {
                        break;
                    }
                    _MEMBER_NOT_FOUND.PrintError();
                    Console.ReadLine();
                    return;
                }
                else if (input == ConsoleKey.N)
                {
                    HandleCreateCustomer(out customer);
                    break;
                }
            }

            Console.Clear();
            do
            {
                _INPUT_LOAN_DAY.Print();

                if (int.TryParse(Console.ReadLine(), out int _loanDay) && _loanDay > 0)
                {
                    loanDay = _loanDay;
                    break;
                }
                else
                {
                    _INVALID_DAY.PrintError();
                }
            } while (true);

            Console.Clear();

            _INPUT_LOAN_BOOKS.PrintLine();
            int i = 1;
            while (true)
            {
                Console.Write($"{i}. ");
                var input = Console.ReadLine();

                if (input is null)
                {
                    _INVALID_BOOK_CODE.PrintError();
                }
                else if (input == "exit")
                {
                    break;
                }
                else
                {
                    var book = _manager.GetBook(input);
                    if (book is null)
                    {
                        _BOOK_NOT_FOUND.PrintError();
                        continue;
                    }
                    i++;
                    books.Add(book);
                }
            }

            Console.Clear();
            _CREATE_LOAN_SUCCESS.PrintSuccess();
            _manager.CreateLoan(customer, loanDay, books);
            Console.ReadLine();
        }
        private void HandleCreateCustomer(out Customer customer)
        {
            Console.Clear();
            _CREATE_NEW_CUSTOMER.PrintLine();

            string name;
            string address;
            string phone;
            string code;

            do
            {
                _INPUT_CUSTOMER_NAME.Print();
                name = Console.ReadLine();
                if (name is null)
                {
                    _INVALID_NAME.PrintError();
                }
            } while (name is null);

            Console.Clear();
            _CREATE_NEW_CUSTOMER.PrintLine();

            do
            {
                _INPUT_CUSTOMER_ADDRESS.Print();
                address = Console.ReadLine();
                if (address is null)
                {
                    _INVALID_ADDRESS.PrintError();
                }
            } while (address is null);

            Console.Clear();
            _CREATE_NEW_CUSTOMER.PrintLine();

            bool invalidPhone;
            do
            {
                _INPUT_CUSTOMER_PHONE.Print();
                phone = Console.ReadLine();
                invalidPhone = phone is null ||
                               phone.Length > 10 ||
                               phone.Length < 9 ||
                               !int.TryParse(phone, out int _);
                if (invalidPhone)
                {
                    _INVALID_PHONE.PrintError();
                }
            } while (invalidPhone);

            _CREATE_CUSTOMER_SUCCESS.PrintSuccess();
            customer = _manager.CreateCustomer(name, address, phone);
            Console.ReadLine();
        }

        private void HandleShowLoanInfo()
        {
            double totalPage = Math.Ceiling(_manager.GetLoans().Count * 1.0 / 5);
            int currentPage = 1;

            ShowLoanInfo(currentPage);
            while (true)
            {
                var input = Console.ReadKey(true).Key;

                if (input == ConsoleKey.D1 && currentPage > 1)
                {
                    currentPage--;
                    ShowLoanInfo(currentPage);
                }
                else if (input == ConsoleKey.D2 && currentPage < totalPage)
                {
                    currentPage++;
                    ShowLoanInfo(currentPage);
                }
                else if (input == ConsoleKey.D3)
                {
                    break;
                }
            }
        }
        private void ShowLoanInfo(int page)
        {
            Console.Clear();
            var loans = _manager.GetLoansPaging(page - 1);
            for (int i = 0; i < 5; i++)
            {
                if (i < loans.Count)
                {
                    Console.WriteLine(loans[i]);
                }
                else
                {
                    Console.WriteLine();
                }
            }

            Console.WriteLine();
            Console.OutputEncoding = Encoding.UTF8;
            _LOAN_LIST_MENU.PrintLine();
            _PREVIOUS_PAGE.PrintLine();
            _NEXT_PAGE.PrintLine();
            _EXIT_TO_MAIN_MENU.PrintLine();
        }
        private void HandleFindBook()
        {
            Console.Clear();
            Console.OutputEncoding = Encoding.UTF8;
            _INPUT_BOOK_CODE.Print();
            string value = Console.ReadLine();
            var foundBook = _manager.GetBook(value);

            if (foundBook is null)
            {
                _BOOK_NOT_FOUND.PrintError();
            }
            else
            {
                Console.WriteLine(foundBook);
            }
            Console.ReadLine();
        }
    }
}
