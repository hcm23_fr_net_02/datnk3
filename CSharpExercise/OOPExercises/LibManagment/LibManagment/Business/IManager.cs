﻿using LibManagment.Domain;

namespace LibManagment.Business
{
    public interface IManager
    {
        void AddNewBook(string title, List<string> authors, double price, string code);
        bool RemoveBook(string id);
        List<Book> GetBooks();
        List<Book> GetBooksPaging(int pageIndex = 0, int elementPerPage = 5);
        Loan CreateLoan(Customer customer, int loanDay, List<Book> books);
        Customer CreateCustomer(string name, string address, string phone);
        Customer? FindCustomer(string value);
        List<Loan> GetLoans();
        List<Loan> GetLoansPaging(int pageIndex = 0, int elementPerPage = 5);
        Book? GetBook(string code);
    }
}
