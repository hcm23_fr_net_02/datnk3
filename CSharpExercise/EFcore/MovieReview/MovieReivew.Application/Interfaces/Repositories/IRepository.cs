﻿using System.Linq.Expressions;

namespace MovieReivew.Application.Interfaces.Repositories
{
    public interface IRepository<T> where T : class
    {
        Task<T?> Get(int id);
        Task<IEnumerable<T>> Find(Expression<Func<T, bool>> predicate);
        Task<T?> SingleOrDefault(Expression<Func<T, bool>> predicate);
        Task Add(T entity);
        Task AddRange(IEnumerable<T> entities);
        Task Update(T entity);
        Task Delete(T entity);
        Task<int> GetCount();
    }
}
