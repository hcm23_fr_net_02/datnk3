﻿namespace MovieReivew.Application.Interfaces.Repositories
{
    public interface IUnitOfWork
    {
        Task SaveChanges();
        IMovieRepository MovieRepository { get; }
        IReviewRepository ReviewRepository { get; }
        IDirectorRepository DirectorRepository { get; }
        INationRepository NationRepository { get; }
    }
}
