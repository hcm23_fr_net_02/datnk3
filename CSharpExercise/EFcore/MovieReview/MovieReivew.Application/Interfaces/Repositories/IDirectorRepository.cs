﻿using MovieReview.Domain.Entities;

namespace MovieReivew.Application.Interfaces.Repositories
{
    public interface IDirectorRepository : IRepository<Director>
    {
        Task<IEnumerable<Director>> GetPaging(int pageNumber, int pageSize);
    }
}
