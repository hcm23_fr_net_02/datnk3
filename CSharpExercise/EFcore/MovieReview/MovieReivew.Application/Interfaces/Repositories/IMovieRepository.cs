﻿using MovieReview.Domain.Entities;

namespace MovieReivew.Application.Interfaces.Repositories
{
    public interface IMovieRepository : IRepository<Movie>
    {
        Task<IEnumerable<Movie>> GetPaging(int pageNumber, int pageSize);
        Task<IEnumerable<Movie>> GetPagingByName(string name, int pageNumber, int pageSize);
        Task<IEnumerable<Movie>> GetPagingByYear(ushort year, int pageNumber, int pageSize);
        Task<IEnumerable<Movie>> GetPagingByNation(string name, int pageNumber, int pageSize);
        Task<IEnumerable<Movie>> GetPagingByDirector(string name, int pageNumber, int pageSize);
        Task<int> GetCountByName(string name);
        Task<int> GetCountByYear(ushort year);
        Task<int> GetCountByNation(string name);
        Task<int> GetCountByDirector(string name);
    }
}
