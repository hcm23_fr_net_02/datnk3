﻿using MovieReview.Domain.Entities;

namespace MovieReivew.Application.Interfaces.Repositories
{
    public interface IReviewRepository : IRepository<Review>
    {
        Task<IEnumerable<Review>> GetPaging(int pageNumber, int pageSize);
        Task<IEnumerable<Review>> GetPagingByMovie(int movieId, int pageNumber, int pageSize);
        Task<int> GetCountByMovie(int movieId);
    }
}
