﻿using MovieReview.Domain.Entities;

namespace MovieReivew.Application.Interfaces.Repositories
{
    public interface INationRepository : IRepository<Nation>
    {
        Task<IEnumerable<Nation>> GetPaging(int pageNumber, int pageSize);
    }
}
