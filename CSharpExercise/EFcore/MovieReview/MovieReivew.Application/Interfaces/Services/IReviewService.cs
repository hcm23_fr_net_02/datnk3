﻿using MovieReview.Domain.Entities;

namespace MovieReivew.Application.Interfaces.Services
{
    public interface IReviewService
    {
        Task<IEnumerable<Review>> GetPaginatedReviews(int pageNumber, int pageSize = 5);
        Task<IEnumerable<Review>> GetPaginatedReviewsByMovie(int movieId, int pageNumber, int pageSize = 5);
        Task AddReview(Review review);
        Task<int> GetCount();
        Task<int> GetCountByMovieId(int movieId);
    }
}
