﻿using MovieReview.Domain.Entities;

namespace MovieReivew.Application.Interfaces.Services
{
    public interface INationService
    {
        Task<IEnumerable<Nation>> GetPaginatedNations(int pageNumber, int pageSize = 5);
        Task<int> GetCount();
    }
}
