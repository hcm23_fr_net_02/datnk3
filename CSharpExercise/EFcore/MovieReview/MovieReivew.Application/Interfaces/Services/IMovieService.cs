﻿using MovieReview.Domain.Entities;

namespace MovieReivew.Application.Interfaces.Services
{
    public interface IMovieService
    {
        Task<IEnumerable<Movie>> GetPaginatedMovies(int pageNumber, int pageSize = 5);
        Task<IEnumerable<Movie>> GetPaginatedMoviesByName(string name, int pageNumber, int pageSize = 5);
        Task<IEnumerable<Movie>> GetPaginatedMoviesByYear(ushort year, int pageNumber, int pageSize = 5);
        Task<IEnumerable<Movie>> GetPaginatedMoviesByDirector(string name, int pageNumber, int pageSize = 5);
        Task<IEnumerable<Movie>> GetPaginatedMoviesByNation(string name, int pageNumber, int pageSize = 5);
        Task<Movie?> GetMovie(int id);
        Task UpdateMovie(Movie movie);
        Task DeleteMovie(int id);
        Task AddMovie(Movie movie);
        Task<int> GetCount();
        Task<int> GetCountByName(string name);
        Task<int> GetCountByYear(ushort year);
        Task<int> GetCountByDirector(string name);
        Task<int> GetCountByNation(string name);
    }
}
