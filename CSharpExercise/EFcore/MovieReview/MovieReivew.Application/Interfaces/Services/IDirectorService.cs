﻿using MovieReview.Domain.Entities;

namespace MovieReivew.Application.Interfaces.Services
{
    public interface IDirectorService
    {
        Task<IEnumerable<Director>> GetPaginatedDirectors(int pageNumber, int pageSize = 5);
        Task<int> GetCount();
    }
}
