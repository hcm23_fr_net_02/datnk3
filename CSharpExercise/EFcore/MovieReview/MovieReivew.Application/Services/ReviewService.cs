﻿using MovieReivew.Application.Interfaces.Repositories;
using MovieReivew.Application.Interfaces.Services;
using MovieReview.Domain.Entities;

namespace MovieReivew.Application.Services
{
    public class ReviewService : IReviewService
    {
        private readonly IUnitOfWork _unitOfWork;

        public ReviewService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task AddReview(Review review)
        {
            try
            {
                await _unitOfWork.ReviewRepository.Add(review);
                await _unitOfWork.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<int> GetCount()
        {
            try
            {
                return await _unitOfWork.ReviewRepository.GetCount();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<int> GetCountByMovieId(int movieId)
        {
            try
            {
                return await _unitOfWork.ReviewRepository.GetCountByMovie(movieId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<Review>> GetPaginatedReviews(int pageNumber, int pageSize = 5)
        {
            try
            {
                return await _unitOfWork.ReviewRepository.GetPaging(pageNumber, pageSize);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<Review>> GetPaginatedReviewsByMovie(int movieId, int pageNumber, int pageSize = 5)
        {
            try
            {
                return await _unitOfWork.ReviewRepository.GetPagingByMovie(movieId, pageNumber, pageSize);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
