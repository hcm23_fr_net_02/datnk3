﻿using MovieReivew.Application.Interfaces.Repositories;
using MovieReivew.Application.Interfaces.Services;
using MovieReview.Domain.Entities;

namespace MovieReivew.Application.Services
{
    public class NationService : INationService
    {
        private readonly IUnitOfWork _unitOfWork;

        public NationService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<int> GetCount()
        {
            try
            {
                return await _unitOfWork.NationRepository.GetCount();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<Nation>> GetPaginatedNations(int pageNumber, int pageSize = 5)
        {
            try
            {
                return await _unitOfWork.NationRepository.GetPaging(pageNumber, pageSize);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
