﻿using MovieReivew.Application.Interfaces.Repositories;
using MovieReivew.Application.Interfaces.Services;
using MovieReview.Domain.Entities;

namespace MovieReivew.Application.Services
{
    public class DirectorService : IDirectorService
    {
        private readonly IUnitOfWork _unitOfWork;

        public DirectorService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task<int> GetCount()
        {
            try
            {
                return await _unitOfWork.DirectorRepository.GetCount();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<Director>> GetPaginatedDirectors(int pageNumber, int pageSize = 5)
        {
            try
            {
                return await _unitOfWork.DirectorRepository.GetPaging(pageNumber, pageSize);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
