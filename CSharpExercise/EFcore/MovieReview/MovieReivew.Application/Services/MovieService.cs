﻿using MovieReivew.Application.Exceptions;
using MovieReivew.Application.Interfaces.Repositories;
using MovieReivew.Application.Interfaces.Services;
using MovieReview.Domain.Entities;

namespace MovieReivew.Application.Services
{
    public class MovieService : IMovieService
    {
        private readonly IUnitOfWork _unitOfWork;

        public MovieService(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public async Task AddMovie(Movie movie)
        {
            try
            {
                await _unitOfWork.MovieRepository.Add(movie);
                await _unitOfWork.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task DeleteMovie(int id)
        {
            try
            {
                var movie = await _unitOfWork.MovieRepository.SingleOrDefault(m => m.Id == id);

                if (movie is null)
                {
                    throw new BusinessLogicException("Cannot find movie id");
                }

                await _unitOfWork.MovieRepository.Delete(movie);
                await _unitOfWork.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<int> GetCount()
        {
            try
            {
                return await _unitOfWork.MovieRepository.GetCount();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<int> GetCountByDirector(string name)
        {
            try
            {
                return await _unitOfWork.MovieRepository.GetCountByDirector(name);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<int> GetCountByName(string name)
        {
            try
            {
                return await _unitOfWork.MovieRepository.GetCountByName(name);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<int> GetCountByNation(string name)
        {
            try
            {
                return await _unitOfWork.MovieRepository.GetCountByNation(name);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<int> GetCountByYear(ushort year)
        {
            try
            {
                return await _unitOfWork.MovieRepository.GetCountByYear(year);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<Movie?> GetMovie(int id)
        {
            try
            {
                var movie = await _unitOfWork.MovieRepository.SingleOrDefault(m => m.Id == id);

                return movie;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<Movie>> GetPaginatedMovies(int pageNumber, int pageSize = 5)
        {
            try
            {
                return await _unitOfWork.MovieRepository.GetPaging(pageNumber, pageSize);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<Movie>> GetPaginatedMoviesByDirector(string name, int pageNumber, int pageSize = 5)
        {
            try
            {
                return await _unitOfWork.MovieRepository.GetPagingByDirector(name, pageNumber, pageSize);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<Movie>> GetPaginatedMoviesByName(string name, int pageNumber, int pageSize = 5)
        {
            try
            {
                return await _unitOfWork.MovieRepository.GetPagingByName(name, pageNumber, pageSize);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<Movie>> GetPaginatedMoviesByNation(string name, int pageNumber, int pageSize = 5)
        {
            try
            {
                return await _unitOfWork.MovieRepository.GetPagingByNation(name, pageNumber, pageSize);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task<IEnumerable<Movie>> GetPaginatedMoviesByYear(ushort year, int pageNumber, int pageSize = 5)
        {
            try
            {
                return await _unitOfWork.MovieRepository.GetPagingByYear(year, pageNumber, pageSize);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task UpdateMovie(Movie movie)
        {
            try
            {
                await _unitOfWork.MovieRepository.Update(movie);
                await _unitOfWork.SaveChanges();
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
