﻿using MovieReivew.Application.Interfaces.Repositories;
using MovieReivew.Application.Services;

namespace MovieReivew.Application.UnitTest.ServiceTests
{
    [TestFixture]
    public class NationServiceTest
    {
        private Mock<IUnitOfWork> _unitOfWork;
        private NationService _nationService;

        [SetUp]
        public void SetUp()
        {
            _unitOfWork = new Mock<IUnitOfWork>();
            _nationService = new NationService(_unitOfWork.Object);
        }

        [Test]
        public async Task GetCount_Success_ReturnNumberOfNations()
        {
            _unitOfWork.Setup(u => u.NationRepository.GetCount()).ReturnsAsync(2);

            int result = await _nationService.GetCount();

            Assert.That(result, Is.EqualTo(2));
        }

        [Test]
        public async Task GetPaginatedNations_FirstPage_ReturnFirst5Nations()
        {
            var returnNations = new List<Nation>
            {
                new Nation { Id = 1 },
                new Nation { Id = 2 },
                new Nation { Id = 3 },
                new Nation { Id = 4 },
                new Nation { Id = 5 }
            };

            _unitOfWork.Setup(u => u.NationRepository.GetPaging(1, 5)).ReturnsAsync(returnNations);

            var result = await _nationService.GetPaginatedNations(1);

            Assert.That(result, Is.EquivalentTo(returnNations));
        }
    }
}
