﻿using MovieReivew.Application.Exceptions;
using MovieReivew.Application.Interfaces.Repositories;
using MovieReivew.Application.Interfaces.Services;
using MovieReivew.Application.Services;
using System.Linq.Expressions;

namespace MovieReivew.Application.UnitTest.ServiceTests
{
    [TestFixture]
    public class MovieServiceTests
    {
        private Mock<IUnitOfWork> _unitOfWork;
        private IMovieService _movieService;

        [SetUp]
        public void SetUp()
        {
            _unitOfWork = new Mock<IUnitOfWork>();
            _movieService = new MovieService(_unitOfWork.Object);
        }

        [Test]
        public async Task GetPaginatedMovies_FirstPage_ReturnFirst5Movies()
        {
            var returnMovies = new List<Movie>()
            {
                new Movie { Id = 1 },
                new Movie { Id = 2 },
                new Movie { Id = 3 },
                new Movie { Id = 4 },
                new Movie { Id = 5 },
            };

            _unitOfWork.Setup(u => u.MovieRepository.GetPaging(1, 5)).ReturnsAsync(returnMovies);

            var result = await _movieService.GetPaginatedMovies(1);

            Assert.That(result, Is.EquivalentTo(returnMovies));
        }

        [Test]
        public async Task GetPaginatedMoviesByName_FirstPage_ReturnFirst5MoviesWithRequiredName()
        {
            var returnMovies = new List<Movie>()
            {
                new Movie { Id = 1, Name = "Sunshire" },
                new Movie { Id = 2, Name = "Hello sun" },
                new Movie { Id = 3, Name = "Heaven in the sun" },
                new Movie { Id = 4, Name = "Sunny day" },
                new Movie { Id = 5, Name = "Sun and Moon" },
            };

            _unitOfWork.Setup(u => u.MovieRepository.GetPagingByName("sun", 1, 5)).ReturnsAsync(returnMovies);

            var result = await _movieService.GetPaginatedMoviesByName("sun", 1);

            Assert.That(result.All(m => m.Name.ToLower().Contains("sun")), Is.True);
        }

        [Test]
        public async Task AddMovie_ValidMovie_NewMovieIsAdded()
        {
            var movie = new Movie { Name = "Home alone" };

            _unitOfWork.Setup(u => u.MovieRepository.Add(It.IsAny<Movie>())).Callback<Movie>(m => m.Id = 1);

            await _movieService.AddMovie(movie);

            _unitOfWork.Verify(u => u.MovieRepository.Add(movie), Times.Once);
            _unitOfWork.Verify(u => u.SaveChanges(), Times.Once);
        }

        [Test]
        public void AddMovie_NullMovie_ThrowException()
        {
            Movie? movie = null;

            Assert.That(async () => await _movieService.AddMovie(movie), 
                    Throws.Exception.TypeOf<NullReferenceException>());
        }

        [Test]
        public async Task DeleteMovie_ExistingMovie_MovieIsDeleted()
        {
            var movie = new Movie { Id = 1, Name = "Home alone" };

            _unitOfWork.Setup(u => u.MovieRepository.SingleOrDefault(It.IsAny<Expression<Func<Movie, bool>>>()))
                       .ReturnsAsync(movie);

            await _movieService.DeleteMovie(1);

            _unitOfWork.Verify(u => u.MovieRepository.Delete(movie), Times.Once);
            _unitOfWork.Verify(u => u.SaveChanges(), Times.Once);
        }

        [Test]
        public void DeleteMovie_NonExistMovie_ThrowException()
        {
            _unitOfWork.Setup(u => u.MovieRepository.SingleOrDefault(It.IsAny<Expression<Func<Movie, bool>>>()))
                       .ReturnsAsync((Movie?)null);

            _unitOfWork.Verify(u => u.SaveChanges(), Times.Never);

            Assert.That(async () => await _movieService.DeleteMovie(2),
                    Throws.Exception.TypeOf<BusinessLogicException>());
        }

        [Test]
        public async Task UpdateMovie_WhenCalled_MovieIsUpdated()
        {
            var movie = new Movie { Id = 5, Name = "Hello Kitty" };

            _unitOfWork.Setup(u => u.MovieRepository.Update(movie)).Returns(Task.CompletedTask);

            await _movieService.UpdateMovie(movie);

            _unitOfWork.Verify(u => u.MovieRepository.Update(movie), Times.Once);
            _unitOfWork.Verify(u => u.SaveChanges(), Times.Once);
        }
    }
}
