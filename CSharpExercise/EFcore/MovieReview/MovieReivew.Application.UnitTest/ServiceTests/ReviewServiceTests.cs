﻿using MovieReivew.Application.Interfaces.Repositories;
using MovieReivew.Application.Interfaces.Services;
using MovieReivew.Application.Services;

namespace MovieReivew.Application.UnitTest.ServiceTests
{
    [TestFixture]
    public class ReviewServiceTests
    {
        private Mock<IUnitOfWork> _unitOfWork;
        private ReviewService _reviewService;

        [SetUp]
        public void SetUp()
        {
            _unitOfWork = new Mock<IUnitOfWork>();
            _reviewService = new ReviewService(_unitOfWork.Object);
        }

        [Test]
        public async Task GetCount_Success_ReturnNumberOfReviews()
        {
            _unitOfWork.Setup(u => u.ReviewRepository.GetCount()).ReturnsAsync(2);

            int result = await _reviewService.GetCount();

            Assert.That(result, Is.EqualTo(2));
        }

        [Test]
        public async Task GetPaginatedReviews_FirstPage_ReturnFirst5Reviews()
        {
            var returnReviews = new List<Review>
            {
                new Review { Id = 1 },
                new Review { Id = 2 },
                new Review { Id = 3 },
                new Review { Id = 4 },
                new Review { Id = 5 }
            };

            _unitOfWork.Setup(u => u.ReviewRepository.GetPaging(1, 5)).ReturnsAsync(returnReviews);

            var result = await _reviewService.GetPaginatedReviews(1);

            Assert.That(result, Is.EquivalentTo(returnReviews));
        }

        [Test]
        public async Task AddReview_ValidReview_NewReviewIsAdded()
        {
            var review = new Review { Title = "hello" };

            _unitOfWork.Setup(u => u.ReviewRepository.Add(It.IsAny<Review>())).Returns(Task.CompletedTask);

            await _reviewService.AddReview(review);

            _unitOfWork.Verify(u => u.ReviewRepository.Add(review), Times.Once);
            _unitOfWork.Verify(u => u.SaveChanges(), Times.Once);
        }

        [Test]
        public void AddReview_NullReview_ThrowException()
        {
            Review? review = null;

            Assert.That(async () => await _reviewService.AddReview(review),
                    Throws.Exception.TypeOf<NullReferenceException>());
        }
    }
}
