﻿using MovieReivew.Application.Interfaces.Repositories;
using MovieReivew.Application.Services;

namespace MovieReivew.Application.UnitTest.ServiceTests
{
    [TestFixture]

    public class DirectorServiceTests
    {
        private Mock<IUnitOfWork> _unitOfWork;
        private DirectorService _directorService;

        [SetUp] 
        public void SetUp()
        {
            // this method is called for each test

            _unitOfWork = new Mock<IUnitOfWork>();
            _directorService = new DirectorService(_unitOfWork.Object);
        }

        [Test]
        public async Task GetCount_Success_ReturnNumberOfDirector()
        {
            _unitOfWork.Setup(u => u.DirectorRepository.GetCount()).ReturnsAsync(2);

            int result = await _directorService.GetCount();

            Assert.That(result, Is.EqualTo(2));
        }

        [Test]
        public async Task GetPaginatedDirectors_FirstPage_ReturnFirst5Directors()
        {
            var returnDirectors = new List<Director>
            {
                new Director { Id = 1 },
                new Director { Id = 2 },
                new Director { Id = 3 },
                new Director { Id = 4 },
                new Director { Id = 5 }
            };

            _unitOfWork.Setup(u => u.DirectorRepository.GetPaging(1, 5)).ReturnsAsync(returnDirectors);

            var result = await _directorService.GetPaginatedDirectors(1);

            Assert.That(result, Is.EquivalentTo(returnDirectors));
        }
    }
}
