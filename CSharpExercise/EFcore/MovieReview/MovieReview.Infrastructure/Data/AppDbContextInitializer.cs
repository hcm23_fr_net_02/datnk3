﻿using Microsoft.EntityFrameworkCore;
using MovieReview.Domain.Entities;
using MovieReview.Infrastructure.Data.Utils;

namespace MovieReview.Infrastructure.Data
{
    public class AppDbContextInitializer
    {
        private readonly AppDbContext _context;
        private readonly string _jsonFilePath = "Data/MockData";

        public AppDbContextInitializer(AppDbContext context)
        {
            _context = context;
        }

        public async Task Initialize()
        {
            try
            {
                //await _context.Database.EnsureCreatedAsync();

                if (_context.Database.GetPendingMigrations().Any())
                {
                    _context.Database.Migrate();
                }

                await SeedDirectors();
                await SeedNations();
                await SeedMovies();
                await SeedReviews();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        private async Task SeedMovies()
        {
            if (await _context.Movies.AnyAsync())
            {
                return;
            }

            List<Movie> movies = await JsonFileReader.ReadAsync<Movie>($"{_jsonFilePath}/movies.json");

            foreach (Movie movie in movies)
            {
                await _context.Movies.AddAsync(new Movie()
                {
                    Name = movie.Name,
                    Description = movie.Description,
                    CreatedDate = movie.CreatedDate,
                    DirectorId = movie.DirectorId,
                    NationId = movie.NationId,
                });
            }

            //await _context.Movies.AddRangeAsync(movies);

            //await _context.Database.ExecuteSqlAsync($"SET IDENTITY_INSERT [MovieReview].[dbo].[Movies] ON;");

            await _context.SaveChangesAsync();

            //await _context.Database.ExecuteSqlAsync($"SET IDENTITY_INSERT [MovieReview].[dbo].[Movies] OFF;");
        }

        private async Task SeedDirectors()
        {
            if (await _context.Directors.AnyAsync())
            {
                return;
            }

            List<Director> directors = await JsonFileReader.ReadAsync<Director>($"{_jsonFilePath}/directors.json");

            foreach (Director director in directors)
            {
                await _context.Directors.AddAsync(new Director()
                {
                    Name= director.Name,
                });
            }

            //await _context.Directors.AddRangeAsync(directors);

            //await _context.Database.ExecuteSqlAsync($"SET IDENTITY_INSERT [MovieReview].[dbo].[Directors] ON;");

            await _context.SaveChangesAsync();

            //await _context.Database.ExecuteSqlAsync($"SET IDENTITY_INSERT [MovieReview].[dbo].[Directors] OFF;");
        }

        private async Task SeedNations()
        {
            if (await _context.Nations.AnyAsync())
            {
                return;
            }

            List<Nation> nations = await JsonFileReader.ReadAsync<Nation>($"{_jsonFilePath}/nations.json");

            foreach (Nation nation in nations)
            {
                await _context.Nations.AddAsync(new Nation()
                {
                    Name = nation.Name,
                });
            }

            //await _context.Nations.AddRangeAsync(nations);

            //await _context.Database.ExecuteSqlAsync($"SET IDENTITY_INSERT [MovieReview].[dbo].[Nations] ON;");

            await _context.SaveChangesAsync();

            //await _context.Database.ExecuteSqlAsync($"SET IDENTITY_INSERT [MovieReview].[dbo].[Nations] OFF;");
        }

        private async Task SeedReviews()
        {
            if (await _context.Reviews.AnyAsync())
            {
                return;
            }

            List<Review> reviews = await JsonFileReader.ReadAsync<Review>($"{_jsonFilePath}/reviews.json");

            foreach (Review review in reviews)
            {
                await _context.Reviews.AddAsync(new Review()
                {
                    Title = review.Title,
                    Description = review.Description,
                    MovieId = review.MovieId,
                    Rating = review.Rating,
                });
            }

            //await _context.Reviews.AddRangeAsync(reviews);

            //await _context.Database.ExecuteSqlAsync($"SET IDENTITY_INSERT [MovieReview].[dbo].[Reviews] ON;");

            await _context.SaveChangesAsync();

            //await _context.Database.ExecuteSqlAsync($"SET IDENTITY_INSERT [MovieReview].[dbo].[Reviews] OFF;");
        }
    }
}
