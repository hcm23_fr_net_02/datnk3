﻿using Microsoft.EntityFrameworkCore;
using MovieReivew.Application.Interfaces.Repositories;
using MovieReview.Domain.Entities;

namespace MovieReview.Infrastructure.Data.Repositories
{
    public class MovieRepository : Repository<Movie>, IMovieRepository
    {
        public MovieRepository(AppDbContext context) : base(context)
        {
        }

        public Task<int> GetCountByDirector(string name)
        {
            var movies = _dbSet.Where(m => m.Director.Name.ToLower().Contains(name.ToLower())).AsNoTracking();

            return movies.CountAsync();
        }

        public Task<int> GetCountByName(string name)
        {
            var movies = _dbSet.Where(m => m.Name.ToLower().Contains(name.ToLower())).AsNoTracking();

            return movies.CountAsync();
        }

        public Task<int> GetCountByNation(string name)
        {
            var movies = _dbSet.Where(m => m.Nation.Name.ToLower().Contains(name.ToLower())).AsNoTracking();

            return movies.CountAsync();
        }

        public Task<int> GetCountByYear(ushort year)
        {
            var movies = _dbSet.Where(m => m.CreatedDate.Year == year).AsNoTracking();

            return movies.CountAsync();
        }

        public async Task<IEnumerable<Movie>> GetPaging(int pageNumber, int pageSize)
        {
            var movies = _dbSet.Skip((pageNumber - 1) * pageSize)
                               .Take(pageSize)
                               .AsNoTracking();

            return await movies.ToListAsync();
        }

        public async Task<IEnumerable<Movie>> GetPagingByDirector(string name, int pageNumber, int pageSize)
        {
            var movies = _dbSet.Where(m => m.Director.Name.ToLower().Contains(name.ToLower()))
                               .Skip((pageNumber - 1) * pageSize)
                               .Take(pageSize)
                               .AsNoTracking();

            return await movies.ToListAsync();
        }

        public async Task<IEnumerable<Movie>> GetPagingByName(string name, int pageNumber, int pageSize)
        {
            var movies = _dbSet.Where(m => m.Name.ToLower().Contains(name.ToLower()))
                               .Skip((pageNumber - 1) * pageSize)
                               .Take(pageSize)
                               .AsNoTracking();

            return await movies.ToListAsync();
        }

        public async Task<IEnumerable<Movie>> GetPagingByNation(string name, int pageNumber, int pageSize)
        {
            var movies = _dbSet.Where(m => m.Nation.Name.ToLower().Contains(name.ToLower()))
                               .Skip((pageNumber - 1) * pageSize)
                               .Take(pageSize)
                               .AsNoTracking();

            return await movies.ToListAsync();
        }

        public async Task<IEnumerable<Movie>> GetPagingByYear(ushort year, int pageNumber, int pageSize)
        {
            var movies = _dbSet.Where(m => m.CreatedDate.Year == year)
                               .Skip((pageNumber - 1) * pageSize)
                               .Take(pageSize)
                               .AsNoTracking();

            return await movies.ToListAsync();
        }
    }
}
