﻿using Microsoft.EntityFrameworkCore;
using MovieReivew.Application.Interfaces.Repositories;
using MovieReview.Domain.Entities;

namespace MovieReview.Infrastructure.Data.Repositories
{
    public class DirectorRepository : Repository<Director>, IDirectorRepository
    {
        public DirectorRepository(AppDbContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Director>> GetPaging(int pageNumber, int pageSize)
        {
            var directors = _dbSet.Skip((pageNumber - 1) * pageSize)
                                  .Take(pageSize)
                                  .AsNoTracking();

            return await directors.ToListAsync();
        }
    }
}
