﻿using MovieReivew.Application.Interfaces.Repositories;

namespace MovieReview.Infrastructure.Data.Repositories
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _appDbContext;

        public UnitOfWork(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;

            MovieRepository = new MovieRepository(_appDbContext);
            ReviewRepository = new ReviewRepository(_appDbContext);
            DirectorRepository = new DirectorRepository(_appDbContext);
            NationRepository = new NationRepository(_appDbContext);
        }

        public IMovieRepository MovieRepository {  get; private set; }

        public IReviewRepository ReviewRepository { get; private set; }

        public IDirectorRepository DirectorRepository { get; private set; }

        public INationRepository NationRepository { get; private set; }

        public async Task SaveChanges()
        {
            await _appDbContext.SaveChangesAsync();
        }
    }
}
