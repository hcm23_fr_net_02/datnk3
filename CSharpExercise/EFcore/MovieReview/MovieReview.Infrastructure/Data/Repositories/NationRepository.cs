﻿using Microsoft.EntityFrameworkCore;
using MovieReivew.Application.Interfaces.Repositories;
using MovieReview.Domain.Entities;

namespace MovieReview.Infrastructure.Data.Repositories
{
    public class NationRepository : Repository<Nation>, INationRepository
    {
        public NationRepository(AppDbContext context) : base(context)
        {
        }

        public async Task<IEnumerable<Nation>> GetPaging(int pageNumber, int pageSize)
        {
            var nations = _dbSet.Skip((pageNumber - 1) * pageSize)
                                .Take(pageSize)
                                .AsNoTracking();

            return await nations.ToListAsync();
        }
    }
}
