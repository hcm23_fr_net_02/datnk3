﻿using Microsoft.EntityFrameworkCore;
using MovieReivew.Application.Interfaces.Repositories;
using MovieReview.Domain.Entities;

namespace MovieReview.Infrastructure.Data.Repositories
{
    public class ReviewRepository : Repository<Review>, IReviewRepository
    {
        public ReviewRepository(AppDbContext context) : base(context)
        {
        }

        public Task<int> GetCountByMovie(int movieId)
        {
            var reviews = _dbSet.Where(r => r.MovieId == movieId).AsNoTracking();

            return reviews.CountAsync();
        }

        public async Task<IEnumerable<Review>> GetPaging(int pageNumber, int pageSize)
        {
            var reviews = _dbSet.Skip((pageNumber - 1) * pageSize)
                                .Take(pageSize)
                                .AsNoTracking();

            return await reviews.ToListAsync();
        }

        public async Task<IEnumerable<Review>> GetPagingByMovie(int movieId, int pageNumber, int pageSize)
        {
            var reviews = _dbSet.Where(r => r.MovieId == movieId)
                                .Skip((pageNumber - 1) * pageSize)
                                .Take(pageSize)
                                .AsNoTracking();

            return await reviews.ToListAsync();
        }
    }
}
