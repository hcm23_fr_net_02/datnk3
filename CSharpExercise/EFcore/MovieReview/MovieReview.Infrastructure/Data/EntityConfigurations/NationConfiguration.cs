﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MovieReview.Domain.Entities;

namespace MovieReview.Infrastructure.Data.EntityConfigurations
{
    internal class NationConfiguration : IEntityTypeConfiguration<Nation>
    {
        public void Configure(EntityTypeBuilder<Nation> builder)
        {
            builder.HasKey(n => n.Id);

            builder.Property(n => n.Id)
                   .ValueGeneratedOnAdd();
        }
    }
}
