﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using MovieReview.Domain.Entities;

namespace MovieReview.Infrastructure.Data.EntityConfigurations
{
    internal class MovieConfiguration : IEntityTypeConfiguration<Movie>
    {
        public void Configure(EntityTypeBuilder<Movie> builder)
        {
            builder.HasKey(m => m.Id);

            builder.Property(m => m.Id)
                   .ValueGeneratedOnAdd();

            builder.Property(m => m.Name)
                   .IsRequired()
                   .HasMaxLength(100);

            builder.Property(m => m.Description)
                   .HasMaxLength(2000);

            builder.HasOne(m => m.Director)
                   .WithMany(d => d.Movies)
                   .HasForeignKey(m => m.DirectorId)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(m => m.Nation)
                   .WithMany(n => n.Movies)
                   .HasForeignKey(m => m.NationId)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.HasMany(m => m.Reviews)
                   .WithOne(r => r.Movie)
                   .HasForeignKey(r => r.MovieId)
                   .OnDelete(DeleteBehavior.Cascade);
        }
    }
}
