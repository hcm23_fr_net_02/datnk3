﻿namespace MovieReview.Domain.Entities
{
    public class Movie
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime CreatedDate { get; set;}
        public int DirectorId { get; set; }
        public Director Director { get; set; }
        public int NationId { get; set; }
        public Nation Nation { get; set; }
        public IList<Review> Reviews { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}; Tựa phim: {Name}; Ngày sản xuất: {CreatedDate.ToLongDateString()}";
        }
    }
}
