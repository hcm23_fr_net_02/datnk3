﻿namespace MovieReview.Domain.Entities
{
    public class Nation
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IList<Movie> Movies { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}; Tên: {Name}";
        }
    }
}
