﻿using MovieReview.Domain.Enums;

namespace MovieReview.Domain.Entities
{
    public class Review
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Rating Rating { get; set; }
        public int MovieId { get; set; }
        public Movie Movie { get; set; }

        public override string ToString()
        {
            return $"Id: {Id}\nTiêu đề: {Title}\nXếp hạng: {(int)Rating}/5\nĐánh giá chi tiết: {Description}\n";
        }
    }
}
