﻿using MovieReivew.Application.Interfaces.Services;
using MovieReview.Presentation.ViewComponents;
using MovieReview.Presentation.ViewComponents.Interfaces;

namespace MovieReview.Presentation
{
    internal class App
    {
        private readonly IReviewService _reviewService;
        private readonly IMovieService _movieService;
        private readonly INationService _nationService;
        private readonly IDirectorService _directorService;

        public App(IMovieService movieService, 
                   IReviewService reviewService,
                   INationService nationService,
                   IDirectorService directorService)
        {
            _movieService = movieService;
            _reviewService = reviewService;
            _nationService = nationService;
            _directorService = directorService;
        }

        public async Task Run()
        {
            IView mainView = new MainView(_movieService, _reviewService, _nationService, _directorService);

            await mainView.Render();
        }
    }
}
