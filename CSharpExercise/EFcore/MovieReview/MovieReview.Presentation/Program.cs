﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MovieReivew.Application.Interfaces.Repositories;
using MovieReivew.Application.Interfaces.Services;
using MovieReivew.Application.Services;
using MovieReview.Infrastructure.Data;
using MovieReview.Infrastructure.Data.Repositories;
using MovieReview.Presentation;
using MovieReview.Presentation.Extensions;

class Program
{
    static async Task Main(string[] args)
    {
        IHostBuilder hostBuilder = Host.CreateDefaultBuilder();

        hostBuilder.ConfigureServices((hostContext, services) =>
        {
            services.AddDbContext<AppDbContext>(options =>
            {
                options.UseSqlServer(hostContext.Configuration.GetConnectionString("DefaultConnection"));
            });

            services.AddScoped<App>();

            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<AppDbContextInitializer>();

            services.AddScoped<IDirectorService, DirectorService>();
            services.AddScoped<IMovieService, MovieService>();
            services.AddScoped<INationService, NationService>();
            services.AddScoped<IReviewService, ReviewService>();
        });

        IHost host = hostBuilder.Build();

        var app = host.Services.GetRequiredService<App>();
        
        await host.InitializeDatabase();

        await app.Run();
    }
}