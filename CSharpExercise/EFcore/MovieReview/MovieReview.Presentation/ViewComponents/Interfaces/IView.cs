﻿namespace MovieReview.Presentation.ViewComponents.Interfaces
{
    internal interface IView
    {
        Task Render();
    }
}
