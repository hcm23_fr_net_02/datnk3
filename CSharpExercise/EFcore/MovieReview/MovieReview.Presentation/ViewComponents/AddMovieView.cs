﻿using MovieReivew.Application.Interfaces.Services;
using MovieReview.Domain.Entities;
using MovieReview.Presentation.Extensions;
using MovieReview.Presentation.ViewComponents.Interfaces;

namespace MovieReview.Presentation.ViewComponents
{
    internal class AddMovieView : IView
    {
        private readonly IMovieService _movieService;
        private readonly IDirectorService _directorService;
        private readonly INationService _nationService;

        private readonly string _ADD_NEW_MOVIE_TITLE = "***** Thêm phim mới *****";
        private readonly string _INPUT_TITLE = "Nhập tựa phim: ";
        private readonly string _INPUT_DIRECTOR_ID = "Nhập id đạo diễn: ";
        private readonly string _INPUT_PUBLISH_YEAR = "Nhập năm sản xuất (mm/dd/yyyy): ";
        private readonly string _INPUT_NATION = "Nhập id quốc gia: ";
        private readonly string _INPUT_DESCRIPTION = "Nhập mô tả: ";

        private readonly string _ADD_MOVIE_SUCCESS = "Thêm phim thành công";

        private readonly string _INVALID_TITLE = "Tựa phim không được rỗng";
        private readonly string _INVALID_PUBLISH_YEAR = "Năm sản xuất không hợp lệ";
        private readonly string _NOT_ALLOW_EMPTY_DIRECTOR = "Không được bỏ trống đạo diễn";
        private readonly string _DIRECTOR_NOT_FOUND = "Không tìm thấy đạo diễn";
        private readonly string _NATION_NOT_FOUND = "Không tìm thấy quốc gia";
        private readonly string _ADD_DIRECTOR_SUCCESS = "Thêm đạo diễn thành công";
        private readonly string _ADD_NATION_SUCCESS = "Thêm quốc gia thành công";
        private readonly string _ADD_YEAR_SUCCESS = "Thêm năm sản xuất thành công";
        private readonly string _DIRECTOR_LIST = "Danh sách các đạo diễn";
        private readonly string _NATION_LIST = "Danh sách các quốc gia";
        private readonly string _INVALID_ID = "Id không hợp lệ";

        public AddMovieView(IMovieService movieService,
                           INationService nationService,
                           IDirectorService directorService)
        {
            _movieService = movieService;
            _nationService = nationService;
            _directorService = directorService;
        }

        public async Task Render()
        {
            Console.Clear();

            _ADD_NEW_MOVIE_TITLE.PrintLine();

            string? title;
            int directorId;
            DateTime year;
            int nationId;
            string? description;

            HandleAddTitle(out title);

            directorId = await HandleAddDirector();

            HandleAddPublishYear(out year);

            nationId = await HandleAddNation();

            HandleAddDescription(out description);

            Movie movie = new Movie()
            {
                Name = title,
                CreatedDate = year,
                NationId = nationId,
                DirectorId = directorId,
                Description = description
            };

            try
            {
                await _movieService.AddMovie(movie);
            }
            catch (Exception ex)
            {
                ex.Message.PrintError();
                Console.ReadLine();
                return;
            }

            _ADD_MOVIE_SUCCESS.PrintSuccess();
            Console.ReadLine();
        }

        private void HandleAddTitle(out string? title)
        {
            do
            {
                _INPUT_TITLE.Print();
                title = Console.ReadLine();

                if (!(string.IsNullOrEmpty(title) || string.IsNullOrWhiteSpace(title)))
                {
                    break;
                }

                _INVALID_TITLE.PrintError();
            } while (title is null);
        }

        private async Task<int> HandleAddDirector()
        {
            int totalDirectors = await _directorService.GetCount();
            double totalPage = Math.Ceiling(totalDirectors * 1.0 / 5);
            int currentPage = 1;

            var directors = await _directorService.GetPaginatedDirectors(currentPage);

            PagingView.PrintItemList(directors, _DIRECTOR_LIST, "Nhập id đạo diễn để thêm", "");

            int inputId;

            while (true)
            {
                ConsoleKey input = Console.ReadKey(true).Key;

                if (input == ConsoleKey.P && currentPage > 1)
                {
                    currentPage--;
                }
                else if (input == ConsoleKey.N && currentPage < totalPage)
                {
                    currentPage++;
                }
                else if (input == ConsoleKey.Enter)
                {
                    Console.WriteLine();

                    _INPUT_DIRECTOR_ID.Print();

                    if (int.TryParse(Console.ReadLine(), out inputId) && 
                        directors.Any(d => d.Id == inputId))
                    {
                        _ADD_DIRECTOR_SUCCESS.PrintSuccess();
                        Console.ReadLine();
                        break;
                    }
                    
                    _INVALID_ID.PrintError();
                    Console.ReadLine();
                }

                directors = await _directorService.GetPaginatedDirectors(currentPage);

                PagingView.PrintItemList(directors, _DIRECTOR_LIST, "Nhập id đạo diễn để thêm", "");
            }

            return inputId;
        }

        private void HandleAddPublishYear(out DateTime year)
        {
            Console.Clear();

            do
            {
                _INPUT_PUBLISH_YEAR.Print();

                if (DateTime.TryParse(Console.ReadLine(), out year))
                {
                    _ADD_YEAR_SUCCESS.PrintSuccess();
                    Console.ReadLine();
                    return;
                }

                _INVALID_PUBLISH_YEAR.PrintError();
            } 
            while (true);
        }

        private async Task<int> HandleAddNation()
        {
            int totalNationId = await _nationService.GetCount();
            double totalPage = Math.Ceiling(totalNationId * 1.0 / 5);
            int currentPage = 1;

            var nations = await _nationService.GetPaginatedNations(currentPage);

            PagingView.PrintItemList(nations, _NATION_LIST, "Nhập id quốc gia để thêm", "");

            int inputId;

            while (true)
            {
                ConsoleKey input = Console.ReadKey(true).Key;

                if (input == ConsoleKey.P && currentPage > 1)
                {
                    currentPage--;
                }
                else if (input == ConsoleKey.N && currentPage < totalPage)
                {
                    currentPage++;
                }
                else if (input == ConsoleKey.Enter)
                {
                    Console.WriteLine();

                    _INPUT_NATION.Print();

                    if (int.TryParse(Console.ReadLine(), out inputId) &&
                        nations.Any(d => d.Id == inputId))
                    {
                        _ADD_NATION_SUCCESS.PrintSuccess();
                        Console.ReadLine();
                        break;
                    }

                    _INVALID_ID.PrintError();
                }

                nations = await _nationService.GetPaginatedNations(currentPage);

                PagingView.PrintItemList(nations, _NATION_LIST, "Nhập id quốc gia để thêm", "");
            }

            return inputId;
        }

        private void HandleAddDescription(out string? description)
        {
            Console.Clear();
            _INPUT_DESCRIPTION.Print();
            description = Console.ReadLine();
        }
    }
}
