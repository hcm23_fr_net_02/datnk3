﻿using MovieReivew.Application.Interfaces.Services;
using MovieReview.Presentation.Extensions;
using MovieReview.Presentation.ViewComponents.Interfaces;
using System.Text;

namespace MovieReview.Presentation.ViewComponents
{
    internal class MainView : IView
    {
        private readonly IReviewService _reviewService;
        private readonly IMovieService _movieService;
        private readonly INationService _nationService;
        private readonly IDirectorService _directorService;

        private readonly IView _movieListView;
        private readonly IView _addMovieView;
        private readonly IView _filterMovieView;

        private readonly string _MAIN_MENU = "********* Menu đánh giá phim *********";
        private readonly string _MOVIE_LIST_VIEW = "   1. Danh sách các bộ phim";
        private readonly string _ADD_MOVIE_VIEW = "   2. Thêm phim mới";
        private readonly string _FILTER_MOVIE_VIEW = "   3. Lọc phim";
        private readonly string _EXIT_PROGRAM = "   4. Thoát";
        private readonly string _INPUT_MENU = "Nhập: ";

        public MainView(IMovieService movieService,
                       IReviewService reviewService,
                       INationService nationService,
                       IDirectorService directorService)
        {
            _movieService = movieService;
            _reviewService = reviewService;
            _nationService = nationService;
            _directorService = directorService;

            _movieListView = new MovieListView(_movieService, _reviewService);
            _addMovieView = new AddMovieView(_movieService, _nationService, _directorService);
            _filterMovieView = new FilterMovieView(_movieService);
        }

        public async Task Render()
        {
            Console.OutputEncoding = Encoding.UTF8;

            while (true)
            {
                ShowMainMenu();

                Console.WriteLine();

                _INPUT_MENU.Print();

                var input = Console.ReadLine();

                if (int.TryParse(input, out int choice))
                {
                    switch (choice)
                    {
                        case 1:
                            await _movieListView.Render();
                            break;
                        case 2:
                            await _addMovieView.Render();
                            break;
                        case 3:
                            await _filterMovieView.Render();
                            break;
                        case 4:
                            break;
                    }
                }

                if (choice == 4)
                {
                    break;
                }
            }
        }

        private void ShowMainMenu()
        {
            Console.Clear();
            _MAIN_MENU.PrintLine();
            _MOVIE_LIST_VIEW.PrintLine();
            _ADD_MOVIE_VIEW.PrintLine();
            _FILTER_MOVIE_VIEW.PrintLine();
            _EXIT_PROGRAM.PrintLine();
        }
    }
}
