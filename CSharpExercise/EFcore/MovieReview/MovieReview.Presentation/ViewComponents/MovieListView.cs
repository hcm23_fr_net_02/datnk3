﻿using MovieReivew.Application.Interfaces.Services;
using MovieReview.Domain.Entities;
using MovieReview.Domain.Enums;
using MovieReview.Presentation.Extensions;
using MovieReview.Presentation.ViewComponents.Interfaces;

namespace MovieReview.Presentation.ViewComponents
{
    internal class MovieListView : IView
    {
        private readonly IMovieService _movieService;
        private readonly IReviewService _reviewService;

        private readonly string _MOVIE_LIST_MENU = "Danh sách phim";
        private readonly string _REVIEW_LIST_MENU = "Danh sách đánh giá";
        private readonly string _ENTER_MESSAGE = "Tác vụ";
        private readonly string _CHOICE_OPTIONS = "Nhấn 1: Xem thông tin chi tiết\n" +
                                                  "Nhấn 2: Chỉnh sửa thông tin\n" +
                                                  "Nhấn 3: Thêm đánh giá\n" +
                                                  "Nhấn 4: Xem tất cả đánh giá\n" +
                                                  "Nhấn 5: Xóa phim\n";
        private readonly string _INPUT_CHOICE = "Nhập lựa chọn: ";
        private readonly string _INPUT_MOVIE_ID = "Nhập id phim: ";
        private readonly string _INVALID_INPUT = "Input không hợp lệ";
        private readonly string _MOVIE_NOT_FOUND = "Không tìm thấy phim";
        private readonly string _MOVIE_DETAIL = "Chi tiết phim";
        private readonly string _REMOVE_MOVIE_FAIL = "Xóa phim thất bại";
        private readonly string _REMOVE_MỌVIE_SUCCESS = "Xóa phim thành công";
        private readonly string _INPUT_MOVIE_NAME = "Nhập tên: ";
        private readonly string _INPUT_YEAR = "Nhập năm sản xuất (mm/dd/yyyy): ";
        private readonly string _INPUT_DESCRIPTION = "Nhập mô tả: ";
        private readonly string _INPUT_MOVIE_DIRECTOR = "Nhập id đạo diễn: ";
        private readonly string _INPUT_MOVIE_NATION = "Nhập id của quốc gia: ";
        private readonly string _UPDATE_SUCCESS = "Cập nhật thành công";
        private readonly string _ADD_SUCCESS = "Thêm thành công";
        private readonly string _INPUT_REVIEW_TITLE = "Nhập title cho đánh giá: ";
        private readonly string _INPUT_REVIEW_DESCRIPTION = "Nhập chi tiết đánh giá: ";
        private readonly string _INPUT_REVIEW_RATING = "Nhập xếp hạng cho phim (1->5): ";

        public MovieListView(IMovieService movieService, IReviewService reviewService)
        {
            _movieService = movieService;
            _reviewService = reviewService;
        }

        public async Task Render()
        {
            Console.Clear();

            int totalMovies = await _movieService.GetCount();
            double totalPage = Math.Ceiling(totalMovies * 1.0 / 5);
            int currentPage = 1;

            var movies = await _movieService.GetPaginatedMovies(currentPage);

            PagingView.PrintItemList(movies, _MOVIE_LIST_MENU, _ENTER_MESSAGE);

            while (true)
            {
                var input = Console.ReadKey(true).Key;

                if (input == ConsoleKey.P && currentPage > 1)
                {
                    currentPage--;
                }
                else if (input == ConsoleKey.N && currentPage < totalPage)
                {
                    currentPage++;
                }
                else if (input == ConsoleKey.B)
                {
                    break;
                }
                else if (input == ConsoleKey.Enter)
                {
                    Console.WriteLine();
                    _CHOICE_OPTIONS.PrintLine();
                    _INPUT_CHOICE.Print();

                    if (ushort.TryParse(Console.ReadLine(), out ushort choice))
                    {
                        if (choice == 1)
                        {
                            await ShowMovieDetail();
                        }
                        else if (choice == 2)
                        {
                            await HandleModifyInfo();
                        }
                        else if (choice == 3)
                        {
                            await HandleAddReview();
                        }
                        else if (choice == 4)
                        {
                            await ShowAllReview();
                        }
                        else if (choice == 5)
                        {
                            await HandleRemoveMovie();
                        }
                    }
                }

                movies = await _movieService.GetPaginatedMovies(currentPage);

                PagingView.PrintItemList(movies, _MOVIE_LIST_MENU, _ENTER_MESSAGE);
            }
        }

        private async Task ShowMovieDetail()
        {
            int movieId;

            _INPUT_MOVIE_ID.Print();

            if (!(int.TryParse(Console.ReadLine(), out movieId) && movieId > 0))
            {
                _INVALID_INPUT.PrintError();

                return;
            }

            try
            {
                Movie? movie = await _movieService.GetMovie(movieId);

                if (movie is null)
                {
                    _MOVIE_NOT_FOUND.PrintError();
                    Console.ReadLine();
                    return;
                }

                Console.Clear();

                _MOVIE_DETAIL.PrintLine();

                Console.WriteLine($"Id: {movie.Id}");
                Console.WriteLine($"Tên: {movie.Name}");
                Console.WriteLine($"Năm sản xuất: {movie.CreatedDate.ToLongDateString()}");
                Console.WriteLine($"Mô tả: {movie.Description}");
                Console.WriteLine($"Mã đạo diễn: {movie.DirectorId}");
                Console.WriteLine($"Mã quốc gia: {movie.NationId}");

                Console.ReadLine();
            }
            catch (Exception ex)
            {
                ex.Message.PrintError();
                return;
            }
        }

        private async Task HandleModifyInfo()
        {
            #region InputMovieId
            int movieId;

            _INPUT_MOVIE_ID.Print();

            if (!(int.TryParse(Console.ReadLine(), out movieId) && movieId > 0))
            {
                _INVALID_INPUT.PrintError();
                Console.ReadLine();
                return;
            }

            Movie? movie = await _movieService.GetMovie(movieId);

            if (movie is null)
            {
                _MOVIE_NOT_FOUND.PrintError();
                Console.ReadLine();
                return;
            }
            #endregion

            Console.Clear();

            string? name;
            DateTime year;
            string? description;

            #region InputName
            _INPUT_MOVIE_NAME.Print();

            name = Console.ReadLine();

            if (string.IsNullOrEmpty(name) || string.IsNullOrWhiteSpace(name))
            {
                movie.Name.PrintLine();
                name = movie.Name;
            }
            #endregion

            #region InputYear
            _INPUT_YEAR.Print();

            if (!DateTime.TryParse(Console.ReadLine(), out year))
            {
                _INVALID_INPUT.PrintError();
                Console.ReadLine();
                return;
            }

            #endregion

            #region InputDescription
            _INPUT_DESCRIPTION.Print();

            description = Console.ReadLine();

            if (string.IsNullOrEmpty(description) || string.IsNullOrWhiteSpace(description))
            {
                description = movie.Description;
            }
            #endregion

            movie.Name = name;
            movie.CreatedDate = year;
            movie.Description = description;

            await _movieService.UpdateMovie(movie);

            _UPDATE_SUCCESS.PrintSuccess();
            Console.ReadLine();
        }

        private async Task HandleAddReview()
        {
            #region InputMovieId
            int movieId;

            _INPUT_MOVIE_ID.Print();

            if (!(int.TryParse(Console.ReadLine(), out movieId) && movieId > 0))
            {
                _INVALID_INPUT.PrintError();
                Console.ReadLine();
                return;
            }

            Movie? movie = await _movieService.GetMovie(movieId);

            if (movie is null)
            {
                _MOVIE_NOT_FOUND.PrintError();
                Console.ReadLine();
                return;
            }
            #endregion

            Console.Clear();

            string? title;
            string? description;
            Rating rating;

            #region InputTitle
            do
            {
                _INPUT_REVIEW_TITLE.Print();

                title = Console.ReadLine();

                if (!(string.IsNullOrEmpty(title) || string.IsNullOrWhiteSpace(title)))
                {
                    break;
                }

                _INVALID_INPUT.PrintError();
            } while (true);
            #endregion

            #region InputDescription
            do
            {
                _INPUT_REVIEW_DESCRIPTION.Print();

                description = Console.ReadLine();

                if (!(string.IsNullOrEmpty(description) || string.IsNullOrWhiteSpace(description)))
                {
                    break;
                }

                _INVALID_INPUT.PrintError();
            } while (true);
            #endregion

            #region InputRating
            do
            {
                _INPUT_REVIEW_RATING.Print();

                if (Enum.TryParse(Console.ReadLine(), out rating) && rating >= Rating.One && rating <= Rating.Five)
                {
                    break;
                }

                _INVALID_INPUT.PrintError();
            } while (true);
            #endregion

            Review review = new()
            {
                Title = title,
                Description = description,
                Rating = rating,
                MovieId = movieId,
            };

            await _reviewService.AddReview(review);

            _ADD_SUCCESS.PrintSuccess();
            Console.ReadLine();
        }

        private async Task ShowAllReview()
        {
            #region InputMovieId
            int movieId;

            _INPUT_MOVIE_ID.Print();

            if (!(int.TryParse(Console.ReadLine(), out movieId) && movieId > 0))
            {
                _INVALID_INPUT.PrintError();
                Console.ReadLine();
                return;
            }

            Movie? movie = await _movieService.GetMovie(movieId);

            if (movie is null)
            {
                _MOVIE_NOT_FOUND.PrintError();
                Console.ReadLine();
                return;
            }
            #endregion

            Console.Clear();

            int totalReviews = await _reviewService.GetCountByMovieId(movieId);
            double totalPage = Math.Ceiling(totalReviews * 1.0 / 5);
            int currentPage = 1;

            var reviews = await _reviewService.GetPaginatedReviewsByMovie(movieId, currentPage);

            PagingView.PrintItemList(reviews, _REVIEW_LIST_MENU);

            while (true)
            {
                var input = Console.ReadKey(true).Key;

                if (input == ConsoleKey.P && currentPage > 1)
                {
                    currentPage--;
                }
                else if (input == ConsoleKey.N && currentPage < totalPage)
                {
                    currentPage++;
                }
                else if (input == ConsoleKey.B)
                {
                    break;
                }

                reviews = await _reviewService.GetPaginatedReviewsByMovie(movieId, currentPage);

                PagingView.PrintItemList(reviews, _REVIEW_LIST_MENU);
            }
        }

        private async Task HandleRemoveMovie()
        {
            int movieId;

            _INPUT_MOVIE_ID.Print();

            if (!(int.TryParse(Console.ReadLine(), out movieId) && movieId > 0))
            {
                _INVALID_INPUT.PrintError();

                return;
            }

            try
            {
                await _movieService.DeleteMovie(movieId);
            }
            catch (Exception ex)
            {
                ex.Message.PrintError();
                Console.ReadLine();
                return;
            }

            _REMOVE_MỌVIE_SUCCESS.PrintSuccess();
            Console.ReadLine();

            return;
        }
    }
}
