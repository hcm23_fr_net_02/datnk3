﻿using MovieReivew.Application.Interfaces.Services;
using MovieReivew.Application.Services;
using MovieReview.Domain.Entities;
using MovieReview.Presentation.Extensions;
using MovieReview.Presentation.ViewComponents.Interfaces;

namespace MovieReview.Presentation.ViewComponents
{
    internal class FilterMovieView : IView
    {
        private readonly IMovieService _movieService;

        private readonly string _FILTER_MENU = "********* Lọc phim theo *********";
        private readonly string _FILTER_WITH_TITLE = "   1. Tựa phim";
        private readonly string _FILTER_WITH_DIRECTOR = "   2. Đạo diễn";
        private readonly string _FILTER_WITH_PUBLISH_YEAR = "   3. Năm sản xuất";
        private readonly string _FILTER_WITH_NATION = "   4. Quốc gia";
        private readonly string _EXIT = "   5. Thoát";
        private readonly string _INPUT_CHOICE = "Nhập: ";

        private readonly string _INPUT_MOVIE_TITLE = "Nhập tên phim: ";
        private readonly string _INPUT_DIRECTOR_NAME = "Nhập tên đạo diễn: ";
        private readonly string _INPUT_PUBLISH_YEAR = "Nhập năm sản xuất: ";
        private readonly string _INPUT_NATION_NAME = "Nhập tên quốc gia: ";

        private readonly string _MOVIE_LIST_BY_TITLE = "Danh sách phim theo tựa đề";
        private readonly string _MOVIE_LIST_BY_DIRECTOR = "Danh sách phim theo đạo diễn";
        private readonly string _MOVIE_LIST_BY_YEAR = "Danh sách phim theo năm sản xuất";
        private readonly string _MOVIE_LIST_BY_NATION = "Danh sách phim theo quốc gia";

        private readonly string _NOT_FOUND = "Không tìm thấy";
        private readonly string _INVALID_INPUT = "Input không hợp lệ";


        public FilterMovieView(IMovieService movieService)
        {
            _movieService = movieService;
        }

        public async Task Render()
        {
            while (true)
            {
                ShowFilterMenu();

                Console.WriteLine();

                _INPUT_CHOICE.Print();

                if (int.TryParse(Console.ReadLine(), out int choice))
                {
                    switch (choice)
                    {
                        case 1:
                            await HandleFilterWithTitle();
                            break;
                        case 2:
                            await HandleFilterWithDirectorAsync();
                            break;
                        case 3:
                            await HandleFilterWithPublishYearAsync();
                            break;
                        case 4:
                            await HandleFilterWithNationAsync();
                            break;
                        case 5:
                            break;
                    }
                }
                if (choice == 5)
                {
                    break;
                }
            }
        }

        private void ShowFilterMenu()
        {
            Console.Clear();
            _FILTER_MENU.PrintLine();
            _FILTER_WITH_TITLE.PrintLine();
            _FILTER_WITH_DIRECTOR.PrintLine();
            _FILTER_WITH_PUBLISH_YEAR.PrintLine();
            _FILTER_WITH_NATION.PrintLine();
            _EXIT.PrintLine();
        }

        private async Task HandleFilterWithTitle()
        {
            Console.Clear();

            string? title;

            do
            {
                _INPUT_MOVIE_TITLE.Print();

                title = Console.ReadLine();

                if (!(string.IsNullOrEmpty(title) || string.IsNullOrWhiteSpace(title)))
                {
                    break;
                }
                _INVALID_INPUT.PrintError();
            } while (true);


            int totalMovies = await _movieService.GetCountByName(title);
            double totalPage = Math.Ceiling(totalMovies * 1.0 / 5);
            int currentPage = 1;

            var movies = await _movieService.GetPaginatedMoviesByName(title, currentPage);

            if (movies.Count() == 0)
            {
                Console.Clear();

                _NOT_FOUND.PrintLine();

                Console.ReadLine();

                return;
            }

            PagingView.PrintItemList(movies, _MOVIE_LIST_BY_TITLE);

            while (true)
            {
                var input = Console.ReadKey(true).Key;

                if (input == ConsoleKey.P && currentPage > 1)
                {
                    currentPage--;
                }
                else if (input == ConsoleKey.N && currentPage < totalPage)
                {
                    currentPage++;
                }
                else if (input == ConsoleKey.B)
                {
                    break;
                }

                movies = await _movieService.GetPaginatedMoviesByName(title, currentPage);

                PagingView.PrintItemList(movies, _MOVIE_LIST_BY_TITLE);
            }
        }

        private async Task HandleFilterWithDirectorAsync()
        {
            Console.Clear();

            string? directorName;

            do
            {
                _INPUT_DIRECTOR_NAME.Print();

                directorName = Console.ReadLine();

                if (!(string.IsNullOrEmpty(directorName) || string.IsNullOrWhiteSpace(directorName)))
                {
                    break;
                }

                _INVALID_INPUT.PrintError();
            } while (true);

            int totalMovies = await _movieService.GetCountByDirector(directorName);
            double totalPage = Math.Ceiling(totalMovies * 1.0 / 5);
            int currentPage = 1;

            var movies = await _movieService.GetPaginatedMoviesByDirector(directorName, currentPage);

            if (movies.Count() == 0)
            {
                Console.Clear();

                _NOT_FOUND.PrintLine();

                Console.ReadLine();

                return;
            }

            PagingView.PrintItemList(movies, _MOVIE_LIST_BY_DIRECTOR);

            while (true)
            {
                var input = Console.ReadKey(true).Key;

                if (input == ConsoleKey.P && currentPage > 1)
                {
                    currentPage--;
                }
                else if (input == ConsoleKey.N && currentPage < totalPage)
                {
                    currentPage++;
                }
                else if (input == ConsoleKey.B)
                {
                    break;
                }

                movies = await _movieService.GetPaginatedMoviesByDirector(directorName, currentPage);

                PagingView.PrintItemList(movies, _MOVIE_LIST_BY_DIRECTOR);
            }
        }

        private async Task HandleFilterWithPublishYearAsync()
        {
            Console.Clear();

            ushort publishYear;

            do
            {
                _INPUT_PUBLISH_YEAR.Print();

                if (ushort.TryParse(Console.ReadLine(), out publishYear))
                {
                    break;
                }

                _INVALID_INPUT.PrintError();
            } while (true);

            int totalMovies = await _movieService.GetCountByYear(publishYear);
            double totalPage = Math.Ceiling(totalMovies * 1.0 / 5);
            int currentPage = 1;

            var movies = await _movieService.GetPaginatedMoviesByYear(publishYear, currentPage);

            if (movies.Count() == 0)
            {
                Console.Clear();

                _NOT_FOUND.PrintLine();

                Console.ReadLine();

                return;
            }

            PagingView.PrintItemList(movies, _MOVIE_LIST_BY_YEAR);

            while (true)
            {
                var input = Console.ReadKey(true).Key;

                if (input == ConsoleKey.P && currentPage > 1)
                {
                    currentPage--;
                }
                else if (input == ConsoleKey.N && currentPage < totalPage)
                {
                    currentPage++;
                }
                else if (input == ConsoleKey.B)
                {
                    break;
                }

                movies = await _movieService.GetPaginatedMoviesByYear(publishYear, currentPage);

                PagingView.PrintItemList(movies, _MOVIE_LIST_BY_YEAR);
            }
        }

        private async Task HandleFilterWithNationAsync()
        {
            Console.Clear();

            string? nationName;

            do
            {
                _INPUT_NATION_NAME.Print();

                nationName = Console.ReadLine();

                if (!(string.IsNullOrEmpty(nationName) || string.IsNullOrWhiteSpace(nationName)))
                {
                    break;
                }

                _INVALID_INPUT.PrintError();
            } while (true);

            int totalMovies = await _movieService.GetCountByNation(nationName);
            double totalPage = Math.Ceiling(totalMovies * 1.0 / 5);
            int currentPage = 1;

            var movies = await _movieService.GetPaginatedMoviesByNation(nationName, currentPage);

            if (movies.Count() == 0)
            {
                Console.Clear();

                _NOT_FOUND.PrintLine();

                Console.ReadLine();

                return;
            }

            PagingView.PrintItemList(movies, _MOVIE_LIST_BY_NATION);

            while (true)
            {
                var input = Console.ReadKey(true).Key;

                if (input == ConsoleKey.P && currentPage > 1)
                {
                    currentPage--;
                }
                else if (input == ConsoleKey.N && currentPage < totalPage)
                {
                    currentPage++;
                }
                else if (input == ConsoleKey.B)
                {
                    break;
                }

                movies = await _movieService.GetPaginatedMoviesByNation(nationName, currentPage);

                PagingView.PrintItemList(movies, _MOVIE_LIST_BY_NATION);
            }
        }

    }
}
