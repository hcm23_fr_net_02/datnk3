﻿using MovieReview.Presentation.Extensions;

namespace MovieReview.Presentation.ViewComponents
{
    internal static class PagingView
    {
        public static void PrintItemList<T>(IEnumerable<T> items,
                                            string title,
                                            string? enterMessage = null,
                                            string? exitMessage = null,
                                            int pageSize = 5)
        {
            Console.Clear();
            int count = items.Count();
            for (int i = 0; i < pageSize; i++)
            {
                if (i < count)
                {
                    Console.WriteLine(items.ElementAt(i));
                }
                else
                {
                    Console.WriteLine();
                }
            }

            Console.WriteLine();
            var result = $"********* {title} *********\n" +
                        $"    P. Trang trước\n" +
                        $"    N. Trang tiếp\n" +
                        (exitMessage is null ? $"    B. Quay lại menu chính\n" : $"    {exitMessage}\n") +
                        (enterMessage is not null ? $"    Enter. {enterMessage}" : "");

            result.PrintLine();
        }
    }
}
