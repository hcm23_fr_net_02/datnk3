﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using MovieReview.Infrastructure.Data;

namespace MovieReview.Presentation.Extensions
{
    public static class InitializerExtensions
    {
        public static async Task InitializeDatabase(this IHost host)
        {
            using var scope = host.Services.CreateScope();

            var initializer = scope.ServiceProvider.GetRequiredService<AppDbContextInitializer>();

            await initializer.Initialize();        
        }
    }
}
