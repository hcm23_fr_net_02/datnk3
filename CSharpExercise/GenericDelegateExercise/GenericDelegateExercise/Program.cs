﻿
delegate T MathOperation<T>(T param1, T param2);
class Program
{
    static void Main(string[] args)
    {
        MathOperation<int> add = Addition;
        Console.WriteLine(PerformOperation(10, 20, add));

        Func<int, int, int> func = Addition;
        Console.WriteLine(PerformOperation2(10, 20, func));

        Func<string, string, string> concat = ConcatString;
        Console.WriteLine(PerformOperation2("Hello", " World!", concat));

    }
    public static T PerformOperation<T>(T param1, T param2, MathOperation<T> operation)
    {
        return operation(param1, param2);
    }

    public static T PerformOperation2<T>(T param1, T param2, Func<T, T, T> operation)
    {
        return operation(param1, param2);
    }

    public static int Addition(int a, int b) { return a + b; }
    public static string ConcatString(string a, string b) {  return a + b; }
}