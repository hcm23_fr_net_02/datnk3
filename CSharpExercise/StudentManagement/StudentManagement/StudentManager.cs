﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement
{
    public class StudentManager
    {
        private List<Student> students;
        public StudentManager()
        {
            students = new List<Student>();
        }
        public void AddStudent(Student student)
        {
            students.Add(student);
        }
        public void RemoveStudent(string name)
        {
            var index = students.FindIndex(x => x.Name == name);
            if (index != -1)
            {
                students.RemoveAt(index);
            }
        }
        public void DisplayStudents()
        {
            foreach (var student in students)
            {
                Console.WriteLine(student.Name);
            }
        }
        public Student? FindStudentByName(string name)
        {
            return students.Find(x => x.Name == name);
        }
    }
}
