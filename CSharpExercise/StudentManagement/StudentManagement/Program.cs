﻿namespace StudentManagement;
class Program
{
    static void Main(string[] args)
    {
        var studentManager = new StudentManager();


        Console.WriteLine("Choose one of these options: ");
        Console.WriteLine("1. Add student");
        Console.WriteLine("2. Remove student");
        Console.WriteLine("3. Display student");
        Console.WriteLine("4. Find student");
        Console.WriteLine("5. Exit");

        Console.Write("Input: ");
        var input = Console.ReadLine();

        while (int.TryParse(input, out int choice))
        {
            switch (choice)
            {
                case 1:
                    Console.Clear();
                    string name;
                    int age;
                    double gpa;
                    Console.Write("Name: ");
                    name = Console.ReadLine();
                    Console.Write("Age: ");
                    age = int.Parse(Console.ReadLine());
                    Console.Write("GPA: ");
                    gpa = double.Parse(Console.ReadLine());
                    var student = new Student(name, age, gpa);
                    studentManager.AddStudent(student);
                    Console.Clear();
                    break;
                case 2:
                    Console.Clear();
                    string deleteName;
                    Console.Write("Name to remove: ");
                    deleteName = Console.ReadLine();
                    studentManager.RemoveStudent(deleteName);
                    Console.Clear();
                    break;
                case 3:
                    Console.Clear();
                    studentManager.DisplayStudents();
                    Console.ReadLine();
                    Console.Clear();
                    break;
                case 4:
                    Console.Clear();
                    string findName;
                    Console.Write("Name to find: ");
                    findName = Console.ReadLine();
                    var foundStudent = studentManager.FindStudentByName(findName);
                    if (foundStudent is not null) Console.WriteLine(foundStudent);
                    else Console.WriteLine("Cannot find the given student");
                    Console.ReadLine();
                    Console.Clear();
                    break;
                default:
                    choice = 5;
                    break;
            }

            if (choice == 5) break;

            Console.WriteLine("Choose one of these options: ");
            Console.WriteLine("1. Add student");
            Console.WriteLine("2. Remove student");
            Console.WriteLine("3. Display student");
            Console.WriteLine("4. Find student");
            Console.WriteLine("5. Exit");

            Console.Write("Input: ");
            input = Console.ReadLine();
        }
        
    }
}