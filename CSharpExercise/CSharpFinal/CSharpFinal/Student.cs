﻿namespace CSharpFinal
{
    public class Student
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int Age { get; set; }
        public double GPA { get; set; }
        public Student(string id, string name, int age, double gpa)
        {
            Id = id;
            Name = name;
            Age = age;
            GPA = gpa;
        }
        public override string ToString()
        {
            return $"Id: {Id}, Name: {Name}, Age: {Age}, GPA: {GPA}";
        }
    }
}
