﻿namespace CSharpFinal;

class Program
{
    static void Main(string[] args)
    {
        var studentsManager = new StudentsManager();

        var viewController = new ViewController(studentsManager);
        viewController.Run();
    }
}