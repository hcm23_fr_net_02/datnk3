﻿using System.Text;

namespace CSharpFinal
{
    public class ViewController
    {

        private IManager manager;

        private const string _MAIN_MENU = "********* Menu chính *********";
        private const string _ADD_STUDENT = "1. Thêm sinh viên (nhập thông tin liên quan đến sinh viên)";
        private const string _SHOW_STUDENT_LIST = "2. Xem danh sách sinh viên (chọn vào thì sẽ ra menu phụ)";
        private const string _FIND_STUDENT = "3. Tìm sinh viên (sau khi chọn thì nhập giá trị tìm kiếm";
        private const string _REMOVE_STUDENT = "4. Xóa sinh viên (nhập mã sinh viên để xóa)";
        private const string _EXIT_PROGRAM = "5. Thoát (dừng chương trình)";

        private const string _STUDENT_LIST_MENU = "********* Menu Xem danh sách sinh viên *********";
        private const string _PREVIOUS_PAGE = "1. Trang trước";
        private const string _NEXT_PAGE = "2. Trang tiếp theo";
        private const string _EXIT_TO_MAIN_MENU = "3. Quay lại menu chính";

        private const string _INPUT_NAME = "Họ và tên: ";
        private const string _INPUT_AGE = "Tuổi: ";
        private const string _INPUT_GPA = "Điểm trung bình (GPA): ";
        private const string _INPUT_ID_NAME = "Nhập Mã sinh viên hoặc Tên: ";
        private const string _INPUT_REMOVE_ID = "Nhập Mã sinh viên cần xóa: ";

        private const string _INVALID_NAME = "Input không hợp lệ, xin mời nhập lại";
        private const string _INVALID_AGE = "Tuổi không hợp lệ, xin mời nhập lại";
        private const string _INVALID_GPA = "GPA không hợp lệ, xin mời nhập lại";
        private const string _ADD_SUCCESS = "Thêm thành công";
        private const string _REMOVE_SUCCESS = "Xóa thành công";
        private const string _REMOVE_FAIL = "Xóa thất bại";
        private const string _NOT_FOUND = "Không tìm thấy";

        public ViewController(IManager _manager)
        {
            manager = _manager;
        }
        public void Run()
        {
            while (true)
            {
                ShowMainMenu();
                var input = Console.ReadLine();
                if (int.TryParse(input, out int choice))
                {
                    switch (choice)
                    {
                        case 1:
                            HandleAddStudent();
                            break;
                        case 2:
                            ShowStudentList();
                            break;
                        case 3:
                            HandleFindStudent();
                            break;
                        case 4:
                            HandleRemoveStudent();
                            break;
                        case 5:
                            break;
                    }
                }
                if (choice == 5) break;
            }
        }
        private void ShowMainMenu()
        {
            Console.Clear();
            Console.OutputEncoding = Encoding.UTF8;
            _MAIN_MENU.PrintLine();
            _ADD_STUDENT.PrintLine();
            _SHOW_STUDENT_LIST.PrintLine();
            _FIND_STUDENT.PrintLine();
            _REMOVE_STUDENT.PrintLine();
            _EXIT_PROGRAM.PrintLine();
        }
        private void ShowStudentList()
        {
            double totalPage = Math.Ceiling(manager.GetStudentsCount() * 1.0 / 5);
            int currentPage = 1;
            ShowStudentListMenu(currentPage);

            while (true)
            {
                var input = Console.ReadKey(true).Key;

                if (input == ConsoleKey.D1 && currentPage > 1)
                {
                    currentPage--;
                    ShowStudentListMenu(currentPage);
                }
                else if (input == ConsoleKey.D2 && currentPage < totalPage)
                {
                    currentPage++;
                    ShowStudentListMenu(currentPage);
                }
                else if (input == ConsoleKey.D3)
                {
                    break;
                }
            }

        }
        private void ShowStudentListMenu(int page)
        {
            Console.Clear();
            var students = manager.GetStudentsPaging(page - 1);
            
            for (int i = 0; i < 5; i++)
            {
                if (i < students.Count)
                {
                    Console.WriteLine(students[i]);
                }
                else
                {
                    Console.WriteLine();
                }
            }

            Console.WriteLine();
            Console.OutputEncoding = Encoding.UTF8;
            _STUDENT_LIST_MENU.PrintLine();
            _PREVIOUS_PAGE.PrintLine();
            _NEXT_PAGE.PrintLine();
            _EXIT_TO_MAIN_MENU.PrintLine();
        }
        private void HandleAddStudent()
        {
            Console.Clear();
            Console.OutputEncoding = Encoding.UTF8;
            string name;
            int age;
            double gpa;

            do
            {
                _INPUT_NAME.Print();
                name = Console.ReadLine();
                if (name is null)
                {
                    _INVALID_NAME.PrintError();
                }
            } while (name is null);

            do
            {
                _INPUT_AGE.Print();
                
                if (int.TryParse(Console.ReadLine(), out int _age) && _age > 0)
                {
                    age = _age;
                    break;
                }
                else
                {
                    _INVALID_AGE.PrintError();
                }
            } while (true);

            do
            {
                _INPUT_GPA.Print();

                if (double.TryParse(Console.ReadLine(),out double _gpa) && _gpa >= 0 && _gpa <= 10)
                {
                    gpa = _gpa;
                    break;
                }
                else
                {
                    _INVALID_GPA.PrintError();
                }
            } while (true);
            

            manager.AddStudent(name, age, gpa);
            _ADD_SUCCESS.PrintSuccess();
            Console.ReadLine();
        }
        private void HandleFindStudent()
        {
            Console.Clear();
            Console.OutputEncoding = Encoding.UTF8;
            _INPUT_ID_NAME.Print();
            string value = Console.ReadLine();
            var foundStudents = manager.GetStudents(value);
            
            foreach (Student student in foundStudents)
            {
                Console.WriteLine(student);
            }
            if (foundStudents.Count == 0)
            {
                _NOT_FOUND.PrintError();
            }
            Console.ReadLine();
        }
        private void HandleRemoveStudent()
        {
            Console.Clear();
            Console.OutputEncoding = Encoding.UTF8;
            _INPUT_REMOVE_ID.Print();
            string id = Console.ReadLine();
            bool isRemoved = manager.RemoveStudent(id);
            if (isRemoved)
            {
                _REMOVE_SUCCESS.PrintSuccess();
            }
            else 
            { 
                _REMOVE_FAIL.PrintError();
            }

            Console.ReadLine();
        }
    }
}
