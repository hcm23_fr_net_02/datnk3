﻿namespace CSharpFinal
{
    public class StudentsManager : IManager
    {
        private List<Student> students;
        private int Count { get; set; }
        public StudentsManager()
        {
            students = new List<Student>();
            Count = 0;
        }
        
        public void AddStudent(string name, int age, double gpa)
        {
            var student = new Student($"{Count}", name, age, gpa);
            Count++;
            students.Add(student);
        }
        public bool RemoveStudent(string id)
        {
            var index = students.FindIndex(x => x.Id == id);
            if (index != -1)
            {
                students.RemoveAt(index);
                return true;
            }
            return false;
        }
        public List<Student> GetStudents(string value) 
        { 
            return students.FindAll(s => s.Name == value || s.Id == value);

        }
        public List<Student> GetAllStudents()
        {
            return students;
        }

        public List<Student> GetStudentsPaging(int pageIndex = 0, int elementPerPage = 5)
        {
            return students.Skip(pageIndex * elementPerPage).Take(elementPerPage).ToList();
        }
        public int GetStudentsCount() { return students.Count; }
    }
}
