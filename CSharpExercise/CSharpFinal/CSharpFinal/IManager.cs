﻿namespace CSharpFinal
{
    public interface IManager
    {
        void AddStudent(string name, int age, double gpa);
        bool RemoveStudent(string id);
        List<Student> GetStudents(string value);
        List<Student> GetAllStudents();
        List<Student> GetStudentsPaging(int pageIndex = 0, int elementPerPage = 5);
        int GetStudentsCount();
    }
}
