﻿namespace DownloadImages;

class Program
{
    public static async Task DownloadDataStream (string url, string filename) 
    {
        var httpClient = new HttpClient();
        Console.WriteLine($"Starting connect {url}");
        try
        {
            HttpResponseMessage response = await httpClient.GetAsync(url);
            response.EnsureSuccessStatusCode();

            using var stream =  await response.Content.ReadAsStreamAsync();

            int SIZEBUFFER = 500;
            using var streamwrite = File.OpenWrite(filename);
            byte[] buffer = new byte[SIZEBUFFER];               

            bool endread = false;
            do
            {
                int numberRead = await stream.ReadAsync(buffer, 0, SIZEBUFFER);
                if (numberRead == 0)
                {
                    endread = true;
                }
                else
                {
                    await streamwrite.WriteAsync(buffer, 0, numberRead);
                }

            } while (!endread);
            Console.WriteLine("Download success");

        }
        catch (Exception e) 
        {
            Console.WriteLine(e.Message);
        }
    }

    static async Task Main(string[] args)
    {
        string path = @"D:\DownloadImages";
        if (!Directory.Exists(path))
            Directory.CreateDirectory(path);

        List<string> imagesUrl = new() {
            "https://static.wikia.nocookie.net/battle-cats/images/a/a2/000_2.png",
            "https://static.wikia.nocookie.net/battle-cats/images/9/9a/001_3.png",
            "https://static.wikia.nocookie.net/battle-cats/images/e/e3/003_3.png"
        };
        
        List<Task> tasks = new();
        foreach (var imageUrl in imagesUrl) 
        {
            var fileName = Path.GetFileName(imageUrl);
            tasks.Add(DownloadDataStream(imageUrl, $@"D:\DownloadImages\{fileName}"));
        }

        await Task.WhenAll(tasks);
    }
}