﻿using System.Net;
using System.Net.Http.Headers;
using System.Net.NetworkInformation;
using System.Text;

namespace TodoApp;

class Program
{
    public static void ShowHeaders(HttpResponseHeaders headers)
    {
        Console.WriteLine("HEADERS:");
        foreach (var header in headers)
        {
            Console.WriteLine($"{header.Key} : {header.Value}");
        }
    }
    public static async Task<string> GetWebContent(string url)
    {
        using var httpClient = new HttpClient();

        try 
        {
            httpClient.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0");
            HttpResponseMessage httpResponseMessage = await httpClient.GetAsync(url);

            ShowHeaders(httpResponseMessage.Headers);
            string html = await httpResponseMessage.Content.ReadAsStringAsync();

            return html;
        }
        catch (Exception e) 
        {
            Console.WriteLine(e.Message);
            return "Error";
        }
    }

    public static async Task<byte[]> DownloadDataBytes(string url)
    {
        using var httpClient = new HttpClient();

        try 
        {
            httpClient.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0");
            HttpResponseMessage httpResponseMessage = await httpClient.GetAsync(url);

            ShowHeaders(httpResponseMessage.Headers);
            var bytes = await httpResponseMessage.Content.ReadAsByteArrayAsync();

            return bytes;
        }
        catch (Exception e) 
        {
            Console.WriteLine(e.Message);
            return null;
        }
    }

    

    static async Task Main(string[] args)
    {
        // var url = "https://www.google.com/search?q=xuanthulab";

        // // var html = await GetWebContent(url);
        // var html = await DownloadDataBytes(url);

        // Console.WriteLine(html);


        // using var httpClient = new HttpClient();
        
        // var httpMessageRequest = new HttpRequestMessage();
        // httpMessageRequest.Method = HttpMethod.Get;
        // httpMessageRequest.RequestUri = new Uri("https://www.google.com.sg/");
        // httpMessageRequest.Headers.Add("User-Agent", "Mozilla/5.0");

        // var httpResponseMessage = await httpClient.SendAsync(httpMessageRequest);

        // var html = await httpResponseMessage.Content.ReadAsStringAsync();
        // Console.WriteLine(html);

        /////////////// POST FORM DATA TO SERVER ///////////////
        // using var httpClient = new HttpClient();
        
        // var httpMessageRequest = new HttpRequestMessage();
        // httpMessageRequest.Method = HttpMethod.Post;
        // httpMessageRequest.RequestUri = new Uri("https://postman-echo.com/post");

        // var parameters = new List<KeyValuePair<string, string>>();
        // parameters.Add(new KeyValuePair<string, string>("key1", "value1"));
        // parameters.Add(new KeyValuePair<string, string>("key2", "value2-1"));
        // parameters.Add(new KeyValuePair<string, string>("key2", "value2-2"));

        // var content = new FormUrlEncodedContent(parameters);
        // httpMessageRequest.Content = content;

        // var httpResponseMessage = await httpClient.SendAsync(httpMessageRequest);

        // ShowHeaders(httpResponseMessage.Headers);

        // var html = await httpResponseMessage.Content.ReadAsStringAsync();
        // Console.WriteLine(html);

        /////////////// POST JSON DATA TO SERVER ///////////////
        using var httpClient = new HttpClient();
        
        var httpMessageRequest = new HttpRequestMessage();
        httpMessageRequest.Method = HttpMethod.Post;
        httpMessageRequest.RequestUri = new Uri("https://postman-echo.com/post");

        string data = @"{
            ""key1"": ""value1"",
            ""key2"": ""value2""
        }";

        var content = new StringContent(data, Encoding.UTF8, "application/json");
        httpMessageRequest.Content = content;

        var httpResponseMessage = await httpClient.SendAsync(httpMessageRequest);

        ShowHeaders(httpResponseMessage.Headers);

        var html = await httpResponseMessage.Content.ReadAsStringAsync();
        Console.WriteLine(html);
    }
}